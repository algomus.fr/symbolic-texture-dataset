"""
This module models three intermediary musical structures
that help retrieving information from music streams.

Main classes:
- Pitches
- Onsets
- Slices

Notes
-----
Implemented structures are designed to describe written scores,
especially from notelist TSV format (See https://github.com/DCMLab/mozart_piano_sonatas
for example), at the scale of individual musical measures.

MusicXML or **kern formats are intended to be supported, but 
further experiments are required to ensure the validity of
the output.

License
-------
This file is part of "Symbolic Texture Dataset" http://algomus.fr/code 
Copyright (C) 2021-2023 by Louis Couturier, 
louis.couturier@u-picardie.fr

"Symbolic Texture Dataset" is free software: you can redistribute it 
and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of 
the License, or (at your option) any later version.

"Symbolic Texture Dataset" is distributed in the hope that it will be 
useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
General Public License for more details.

You should have received a copy of the GNU General Public License 
along with "Symbolic Texture Dataset". If not, see
http://www.gnu.org/licenses/
"""

import fractions

import music21 as m21
import copy



#########
# Utils #
#########


def get_beat_duration(stream):
    """
    Return the length of a beat in given stream,
    in number of quarter notes.

    Parameters
    ---------
    stream : music21.stream.Stream object
    """
    return stream.recurse().getElementsByClass(
        m21.meter.TimeSignature)[-1].beatDuration.quarterLength


def stats(values):
    """
    Return statistics on given numeric values.

    Parameters
    ----------
    values : list of numbers (int or float)
        Values to analyze.

    Returns
    -------
    avg_value : float
        Average of given values.
    std_value : float
        Standard deviation.
    min_value : number (int or float)
        Minimum value.
    max_value : number (int or float)
        Maximum value.
    med_value : number (int or float)
        Median value, interpolated if there is
        an even number of values.
    """
    # Check argument
    if not values:
        raise ValueError("'values' argument must not be empty")
    len_values = len(values)
    sorted_values = sorted(values)

    # Compute average value and standard deviation
    avg_value = float(sum(values) / len_values)
    std_value = float(
        sum([abs(value-avg_value) for value in values])
        / len_values
    )

    # Compute mininum, maximum and median
    min_value = sorted_values[0]
    max_value = sorted_values[-1]
    if len_values % 2 == 0:
        med_value = (
            sorted_values[int(len_values/2)]
            + sorted_values[int(len_values/2 - 1)]
        ) / 2
    else:
        med_value = sorted_values[int((len_values-1)/2)]

    return avg_value, std_value, min_value, max_value, med_value

def weighted_stats(weighted_values):
    """
    Return statistics on given numeric weighted values.

    Parameters
    ----------
    values : list of tuple
        Values to analyze, with their weight. Each
        element of the given list must be a tuple
        (value, weight), where value and weight are
        typed as int or float.

    Returns
    -------
    avg_value : float
        Average of given values.
    std_value : float
        Standard deviation.
    min_value : number (int or float)
        Minimum value.
    max_value : number (int or float)
        Maximum value.
    med_value : number (int or float)
        Weighted median.
    """
    # Check argument
    if not weighted_values:
        raise ValueError("'values' argument must not be empty")
    len_values = len(weighted_values)
    sorted_weighted_values = sorted(weighted_values, key=lambda x: x[0])

    # Compute average value
    sum_values = 0.
    total_weight = 0.
    for value, weight in sorted_weighted_values:
        sum_values += value * weight
        total_weight += weight
    avg_value = float(sum_values / total_weight)

    # Compute standard deviation
    sum_values = 0.
    for value, weight in sorted_weighted_values:
        sum_values += abs(value-avg_value) * weight
    std_value = float(sum_values / total_weight)

    # Compute mininum and maximum
    min_value = sorted_weighted_values[0][0]
    max_value = sorted_weighted_values[-1][0]

    # Compute median
    med_weight = total_weight / 2
    cumulative_weight = 0
    index_med = 0
    med_value = None
    ## Remove values with no weight
    sorted_weighted_values = [
        weighted_value
        for weighted_value in sorted_weighted_values 
        if weighted_value[1] > 1e-3 # Minimal weight
    ]
    ## Loop over values
    while cumulative_weight < med_weight:
        cumulative_weight += sorted_weighted_values[index_med][1]
        index_med += 1
        if abs(cumulative_weight - med_weight) < 1e-3:
            # Case of equality: median is the average of two successive terms
            med_value = (
                sorted_weighted_values[index_med-1][0]
                + sorted_weighted_values[index_med][0]
            ) / 2
    med_value = med_value or sorted_weighted_values[index_med-1][0]

    return avg_value, std_value, min_value, max_value, med_value



################
# Main classes #
################

class Pitches:
    """Class to model a set of notes in a stream.

    It models each note written in the stream by 
    their pitch and duration.

    Attributes
    ----------
    pitch_dict : dict {<midi_pitch:int>: list of <duration_quarter:float>}
        Dictionary of notes, grouped by pitch (as MIDI identifier,
        60 for C4). Values of the dictionary are lists of durations of all the
        notes with the same pitch, given in number of quarter notes.
    length_quarter : float
        Duration of the original stream, in number of quarter notes.
    length_beat : float
        Duration of the original stream, in number of beats.
    beat_duration : float
        Duration of a single beat in the original stream, in number of
        quarter notes.

    Notes
    -----
    Grace notes are also taken into account, with
    duration 0.
    """
    def __init__(self, stream, length=None, beat_duration=None):
        if length:
            self.length_quarter = length
        else:
            # Compute length from given stream
            if isinstance(stream, m21.stream.Stream):
                self.length_quarter = stream.duration.quarterLength
            elif type(stream) == list:
                numerator, denominator = stream[0]['timesig'].split('/')
                self.length_quarter = (
                    4 * float(numerator) / float(denominator)
                )

        if beat_duration:
            self.beat_duration = beat_duration
        else:
            # Compute the duration of one beat from given stream
            if isinstance(stream, m21.stream.Stream):
                self.beat_duration = get_beat_duration(stream)
            elif type(stream) == list:
                timesig_to_beat = {
                    '2/2': 2.0,
                    '2/4': 1.0, '3/4': 1.0, '4/4': 1.0,
                    '3/8': 0.5,
                    '6/8': 1.5, '9/8': 1.5, '12/8': 1.5,
                }
                self.beat_duration = timesig_to_beat[stream[0]['timesig']]
        self.length_beat = self.length_quarter / self.beat_duration

        # Build main structure
        self.pitch_dict = {}
        if isinstance(stream, m21.stream.Stream):
            for element in stream.recurse().notes:
                if isinstance(element, m21.note.Note):
                    if element.pitch.midi in self.pitch_dict:
                        # Pitch already known
                        self.pitch_dict[element.pitch.midi].append(
                            element.quarterLength)
                    else:
                        # New pitch: not already encountered
                        self.pitch_dict[element.pitch.midi] = [
                            element.quarterLength]
                elif isinstance(element, m21.chord.Chord):
                    for note in element.notes:
                        if note.pitch.midi in self.pitch_dict:
                            # Pitch already known
                            self.pitch_dict[note.pitch.midi].append(
                                note.quarterLength)
                        else:
                            # New pitch: not already encountered
                            self.pitch_dict[note.pitch.midi] = [
                                note.quarterLength]
        elif type(stream) == list:
            # List of notes dictionaries
            for note in stream:
                # Compute note duration in number of quarter
                duration_fraction = note['duration'].split('/')
                if len(duration_fraction) == 2:
                    note_quarter_duration = (
                        4*float(duration_fraction[0])
                        / float(duration_fraction[-1])
                    )
                elif len(duration_fraction) == 1:
                    note_quarter_duration = 4*float(duration_fraction[0])
                
                # Add note
                midi_pitch = int(note['midi'])
                if midi_pitch in self.pitch_dict:
                    # Pitch already known
                    self.pitch_dict[midi_pitch].append(
                        note_quarter_duration)
                else:
                    # New pitch: not already encountered
                    self.pitch_dict[midi_pitch] = [note_quarter_duration]
        else:
            raise ValueError("'stream' argument must be as music21 Stream or a list of note dictionaries.")

    def __repr__(self):
        return str(self.pitch_dict)

    ## Secondary methods

    def length(self, unit):
        """
        Return the length of the original stream
        in given unit of time.

        Parameters
        ----------
        unit : str in {"quarter", "beat"}
            If "beat" is given, the length is returned in number of quarter notes.
            If "beat" is given, the length is returned in number of beats.
            Shorthands "q" and "b" can be used.

        Returns
        -------
        float
            Duration of the original stream.
        """
        if unit in ["quarter", "q"]:
            return self.length_quarter
        elif unit in ["beat", "b"]:
            return self.length_beat
        else:
            raise ValueError("'unit' argument must be 'quarter' or 'beat' (or shorthand 'q' or 'b')")

    def pitch_list(self):
        """
        Return the list of all distinct pitches in current object,
        as MIDI identifiers (60 for C4, 61 for C#4/Db4...).

        Returns
        -------
        list of int
        """
        return list(self.pitch_dict.keys())

    def duration_list(self, unit='beat'):
        """
        Return the list of the durations of all the notes
        in current object, sorted in descending order.

        Parameters
        ----------
        unit : str in {"quarter", "beat"}
            If "beat" is given, time values are given in number of quarter notes.
            If "beat" is given, time values are given in number of beats.
            Shorthands "q" and "b" can be used.
            Defaults to "beat".
        
        Returns
        -------
        list of float
        """
        all_durations = sorted(
            sum(self.pitch_dict.values(), []),
            reverse=True
        )
        if unit in ["beat", "b"]:
            return [duration/self.beat_duration for duration in all_durations]
        elif unit in ["quarter", "q"]:
            return all_durations
        else:
            raise ValueError("'unit' argument must be 'quarter' or 'beat' (or shorthand 'q' or 'b')")

    def weighted_pitch_list(self):
        """
        Return the list of all distinct pitches, weighted by their
        total duration in the stream, in number of quarter notes.

        Returns
        -------
        list of tuple
            Tuples are in the form (<midi_pitch:int>, <total_duration_quarter:float>)
        """
        return [
            (pitch, sum(durations))
            for pitch, durations
            in self.pitch_dict.items()
        ]

    ## Descriptors

    def n_notes(self, norm_by_length=True):
        """
        Return the total number of notes in the stream,
        divided by the length of the stream in number of beats.
        Grace notes are taken into account.

        Returns
        -------
        float
        """
        n_notes = len(self.duration_list())
        if norm_by_length:
            n_notes /= self.length_beat
        return n_notes

    def n_pitches(self, norm_by_length=True):
        """
        Return the total number of distinct pitches in the stream,
        divided by the length of the stream in number of beats.
        Grace notes are taken into account.

        Returns
        -------
        float
        """
        n_pitches = len(self.pitch_dict)
        if norm_by_length:
            n_pitches /= self.length_beat
        return n_pitches

    def n_pitchclasses(self, norm_by_length=True):
        """
        Return the total number of distinct pitch classes in the stream,
        divided by the length of the stream in number of beats.
        Grace notes are taken into account.

        Returns
        -------
        float
        """
        n_pitchclasses = len(set( # Keep only unique pitch classes
            [midi_pitch%12 for midi_pitch in self.pitch_list()]
        ))
        if norm_by_length:
            n_pitchclasses /= self.length_beat
        return n_pitchclasses

    def novelty(self):
        """
        Return the proportion of notes that have a
        pitch that has not been played in the stream before.

        Returns
        -------
        float

        Notes
        -----
        For empty streams, the default value is 1. because we want
        the opposite of 'novelty' to measure the importance of
        repetition in the stream. Hence, there is no repeated pitches
        in empty streams.
        """
        if not self.pitch_dict:
            # Empty stream
            return 1.
        total_new_pitch = self.n_pitches(norm_by_length=False)
        total_notes = len(self.duration_list())
        return total_new_pitch / total_notes

    def stats_pitches(self):
        """
        Return statistics about midi pitch values.

        Returns
        -------
        float
            Average pitch in the stream, weighted by duration.
        float
            Standard deviation of pitches.
        int
            Minimum value in the stream.
        int
            Maximum value in the stream.
        number (int or float)
            Weighted median pitch.
        """
        if not self.pitch_dict:
            return None, None, None, None, None
        return weighted_stats(self.weighted_pitch_list())

    def stats_durations(self, unit='beat'):
        """
        Return statistics on the durations of the notes,
        in given unit.

        Returns
        -------
        float
            Average duration of notes in the stream.
        float
            Standard deviation of durations.
        float
            Minimum duration in the stream.
        float
            Maximum duration in the stream.
        float
            Median duration, interpolated if there is
            an even number of notes.
        """
        if not self.pitch_dict:
            # Empty stream
            return 0., 0., 0., 0., 0.
        return stats(self.duration_list(unit=unit))


class Onsets:
    """Class to model onsets in a stream.

    Onsets are events involving at least one new note
    being played. The duration of notes are not taken
    into account in this structure.

    Attributes
    ----------
    onset_dict : dict {<onset_time:float>: list of <midi_pitch:int>}
        Dictionary of onsets, grouping new notes (as MIDI pitches, 60 for C4)
        by their onset time, in number of quarter notes since the beginning of
        the original stream.
    length_quarter : float
        Duration of the original stream, in number of quarter notes.
    length_beat : float
        Duration of the original stream, in number of beats.
    beat_duration : float
        Duration of a single beat in the original stream, in number of
        quarter notes.

    Notes
    -----
    Grace notes are not ignored in onsets.
    """
    def __init__(self, stream, length=None, beat_duration=None):
        if length:
            self.length_quarter = length
        else:
            # Compute length from given stream
            self.length_quarter = stream.duration.quarterLength

        if beat_duration:
            self.beat_duration = beat_duration
        else:
            # Compute the duration of one beat from given stream
            self.beat_duration = get_beat_duration(stream)
        self.length_beat = self.length_quarter / self.beat_duration

        # Build main structure
        self.onset_dict = {}
        if isinstance(stream, m21.stream.Stream):
            for element in stream.recurse().notes:
                if isinstance(element.offset, fractions.Fraction):
                    # Handle specific case of fractional onsets
                    onset_key = str(element.offset.numerator
                        / element.offset.denominator)[:4]
                else:
                    onset_key = str(element.offset)[:4] # Limit up to 2 decimals

                if isinstance(element, m21.note.Note):
                    if element.duration.isGrace:
                        continue # Skip grace notes
                    elif onset_key in self.onset_dict:
                        # WARNING: the "offset" is the time of the beginning
                        # of the notes/the onset
                        # New offset time: not already encountered
                        self.onset_dict[onset_key].append(
                            element.pitch.midi)
                    else:
                        # Offset time already known
                        self.onset_dict[onset_key] = [element.pitch.midi]
                elif isinstance(element, m21.chord.Chord):
                    for note in element.notes:
                        if note.duration.isGrace:
                            continue # Skip grace notes
                        elif onset_key in self.onset_dict:
                            # New onset time: not already encountered
                            self.onset_dict[onset_key].append(
                                note.pitch.midi)
                        else:
                            # Offset time already known
                            self.onset_dict[onset_key] = [note.pitch.midi]
        elif type(stream) == list:
            # Then, given argument is a list of notes dictionaries
            for note in stream:
                if float(note['duration'].split('/')[0]) < 10e-5:
                    # Skip grace notes (with duration 0.)
                    continue

                # Convert onset value in number of quarter notes
                onset_fraction = note['onset'].split('/')
                if len(onset_fraction) == 2:
                    onset_quarter = str(
                        4*float(onset_fraction[0])
                        / float(onset_fraction[-1])
                    )[:4] # Limite the size up to 2 decimals
                elif len(onset_fraction) == 1:
                    onset_quarter = str(4*float(onset_fraction[0]))[:4]

                # Add note
                if onset_quarter in self.onset_dict:
                    # Onset already known
                    self.onset_dict[onset_quarter].append(
                        int(note['midi']))
                else:
                    # New pitch: not already encountered
                    self.onset_dict[onset_quarter] = [int(note['midi'])]
        else:
            raise ValueError("'stream' argument must be as music21 Stream or a list of note dictionaries.")

    def __repr__(self):
        return str(self.onset_dict)

    ## Secondary methods

    def length(self, unit):
        """
        Return the length of the original stream
        in given unit of time.

        Parameters
        ----------
        unit : str in {"quarter", "beat"}
            If "beat" is given, the length is returned in number of quarter notes.
            If "beat" is given, the length is returned in number of beats.
            Shorthands "q" and "b" can be used.

        Returns
        -------
        float
            Duration of the original stream.
        """
        if unit in ["quarter", "q"]:
            return self.length_quarter
        elif unit in ["beat", "b"]:
            return self.length_beat
        else:
            raise ValueError("'unit' argument must be 'quarter' or 'beat' (or shorthand 'q' or 'b')")

    def time_list(self, unit="quarter", to_float=True):
        """
        Return the list of offset times, the number of time units
        between the onset and the beginning of the stream, in
        chronological order.

        Notes
        -----
        Warning: while the term "onset" refers to the beginning of a note,
        the term "offset" is not its opposite, here, but the position of
        the object in the stream, as used in music21 (http://web.mit.edu/music21/doc/moduleReference/moduleBase.html#music21.base.Music21Object.offset).
        In other words, the "offset" of a note, is the moment when this note
        is played, expressed in quarter length, since the beginning of the
        extract.

        Parameters
        ----------
        unit : str in {"quarter", "beat"}
            If "beat" is given, time values are given in number of quarter notes.
            If "beat" is given,time values are given in number of beats.
            Shorthands "q" and "b" can be used.
            Defaults to "quarter".
        to_float : bool
            If True, the elements of the returned list are given as float. Else,
            the values are kept as strings. Defaults to True.
        
        Returns
        -------
        list of onset times
        """
        if to_float:
            onset_times = sorted([float(time) for time in self.onset_dict.keys()])
        else:
            onset_times = sorted(list(self.onset_dict.keys()))
        if unit in ["quarter", "q"]:
            return onset_times
        elif unit in ["beat", "b"]:
            return [onset_time/self.beat_duration for onset_time in onset_times]
        else:
            raise ValueError("'unit' argument must be 'quarter' or 'beat' (or shorthand 'q' or 'b')")

    def duration_list(self, unit="quarter"):
        """
        Return the list of duration between each successive onset.
        The last element of the list is the duration between the last onset
        and the end of the stream.

        Parameters
        ----------
        unit : str in {"quarter", "beat"}
            If "beat" is given, time values are given in number of quarter notes.
            If "beat" is given,time values are given in number of beats.
            Shorthands "q" and "b" can be used.
            Defaults to "quarter".

        Returns
        -------
        list of float
        """
        if not self.onset_dict:
            # Empty measure
            return [self.length(unit=unit)]

        times = self.time_list(unit=unit)
        times.append(self.length(unit=unit))
        durations = []
        for i in range(1, len(times)):
            durations.append(times[i]-times[i-1])
        return durations

    def chord_list(self):
        """
        Return the list of all onsets as groups of new pitches that are played.

        Returns
        -------
        list of (list of int)
            Pitches are given in MIDI: 60 for C4.
        """
        return list(self.onset_dict.values())

    ## Descriptors

    def n_onsets(self, norm_by_length=True):
        """
        Return the total number of onsets in the stream,
        divided by the length of the stream in number of beats.

        Returns
        -------
        float
        """
        n_onsets = len(self.onset_dict)
        if norm_by_length:
            n_onsets /= self.length_beat
        return n_onsets

    def stats_regularity(self, unit='beat'):
        """
        Return statistics on the duration between
        successive onsets, in given unit.

        Parameters
        ----------
        unit : str in {"quarter", "beat"}
            If "beat" is given, time values are given in number of quarter notes. 
            If "beat" is given,time values are given in number of beats. 
            Shorthands "q" and "b" can be used. 
            Defaults to "beat".

        Returns
        -------
        float
            Average duration between successive onsets in the stream.
        float
            Standard deviation.
        float
            Minimal time between successive onsets in the stream.
        float
            Maximal time between successive onsets in the stream.
        float
            Median duration between successive onsets in the stream.
        """
        return stats(self.duration_list(unit=unit))

    def stats_n_voices(self):
        """
        Return statistics on the number of voices in onsets.

        Here, we call number of voices the number of threads or distinct 
        pitches played simultaneously in an onset.

        Returns
        -------
        float
            Average number of voices in the onsets.
        float
            Standard deviation.
        int
            Minimal number of voices in the onsets.
        int
            Maximal number of voices in the onsets.
        number (int or float)
            Median number of voices in the onsets.
        """
        n_voices_list = [len(chord) for chord in self.chord_list()]
        if not n_voices_list:
            return 0., 0., 0, 0, 0
        return stats(n_voices_list)

    def stats_n_gaps(self):
        """
        Return statistics on the number of gaps in onsets.

        The "gaps" are the harmonic intervals (i.e. between notes that are
        played simultaneously, in the same onset), that are strictly wider
        than a perfect fourth (6 semitones or more), as defined by 
        Q. R. Nordgren.

        Returns
        -------
        float
            Average number of gaps in the onsets.
        float
            Standard deviation.
        int
            Minimal number of gaps in the onsets.
        int
            Maximal number of gaps in the onsets.
        number (int or float)
            Median number of gaps in the onsets.

        References
        ----------
        Q. R. Nordgren. A measure of textural patterns and strengths. 
        J. of Music Theory, 4(1):19–31, Apr. 1960
        """
        n_gaps_list = []
        for chord in self.chord_list():
            n_gaps = 0
            all_pitches = sorted(chord)
            for i in range(len(chord)-1):
                if all_pitches[i+1]-all_pitches[i] > 5: # 5 semitones = fourth
                    # Gap detected
                    n_gaps += 1
            n_gaps_list.append(n_gaps)
        if not n_gaps_list:
            return 0., 0., 0, 0, 0
        return stats(n_gaps_list)

    def harmonic_intervals(self):
        """
        Return average proportions of common intervals among notes from
        the same onsets.

        Returns
        -------
        prop_third_sixth : float
            Proportion of thirds and sixths among all harmonic interval 
            existing between notes in the same onsets. Averaged on the 
            onsets.
        prop_fourth_fifth : float
            Proportion of perfect fourth and fifth among all harmonic 
            interval existing between notes in the same onsets. 
            Averaged on the onsets.
        prop_octave : float
            Proportion of perfect octave among all harmonic interval 
            existing between notes in the same onsets.  Averaged on 
            the onsets.
        """
        if not self.onset_dict:
            # Empty stream
            return 0., 0., 0.

        total_third_sixth = 0
        total_fourth_fifth = 0
        total_octave = 0
        for chord in self.chord_list():
            has_third_or_sixth = False
            has_fourth_or_fifth = False
            has_octave = False
            # Check each combination
            for pitch1 in range(len(chord)):
                for pitch2 in range(1+pitch1, len(chord)):
                    interval = abs(chord[pitch2]-chord[pitch1])
                    if interval in [3, 4, 8, 9]:
                        has_third_or_sixth = True
                    elif interval in [5, 7]:
                        has_fourth_or_fifth = True
                    elif interval == 12:
                        has_octave = True
            # Add results (+1 if True, else +0)
            total_third_sixth += has_third_or_sixth
            total_fourth_fifth += has_fourth_or_fifth
            total_octave += has_octave
        # Normalize by the number of onsets
        prop_third_sixth = total_third_sixth / len(self.onset_dict)
        prop_fourth_fifth = total_fourth_fifth / len(self.onset_dict)
        prop_octave = total_octave / len(self.onset_dict)

        return prop_third_sixth, prop_fourth_fifth, prop_octave


class Slices:
    """Class to model successive slices in a stream.

    Slices (/temporal segments) are delimited by the appearance or 
    disappearance of notes in the stream. Each slice is matched to all the 
    pitches that are heard during this time (newly played or sustained from 
    previous onsets, indifferently). This process is sometimes reffered to 
    as chordifying or 'salami slicing'.

    Attributes
    ----------
    slice_dict : dict {<start_time:float>: list of <midi_pitch:int>}
        Dictionary of slices, grouping notes that are currently heard
        (as MIDI pitches, 60 for C4) in each slice, referenced by its
        starting time, in number of quarter notes since the beginning of
        the original stream.
    length_quarter : float
        Duration of the original stream, in number of quarter notes.
    length_beat : float
        Duration of the original stream, in number of beats.
    beat_duration : float
        Duration of a single beat in the original stream, in number of
        quarter notes.

    Notes
    -----
    Grace notes are not ignored in slices.
    Slices class differs from Onsets by considering all the notes that are
    played at each moment. It does not only consider new notes, but also
    their duration. It can even capture the absence of notes with empty lists
    of pitches.
    """
    def __init__(self, stream, length=None, beat_duration=None):
        if length:
            self.length_quarter = length
        else:
            # Compute length from given stream
            self.length_quarter = stream.duration.quarterLength

        if beat_duration:
            self.beat_duration = beat_duration
        else:
            # Compute the duration of one beat from given stream
            self.beat_duration = get_beat_duration(stream)
        self.length_beat = self.length_quarter / self.beat_duration

        # Build main structure
        self.slice_dict = {}
        if isinstance(stream, m21.stream.Stream):
            ## Remove grace notes
            stream_copy = copy.deepcopy(stream)
            grace_notes = []
            for n in stream_copy.recurse().notes:
                if n.duration.isGrace:
                    grace_notes.append(n)
            for grace in grace_notes:
                grace.activeSite.remove(grace)

            ## Loop over remaining slices
            for element in stream_copy.chordify().recurse().notesAndRests:
                if isinstance(element.offset, fractions.Fraction):
                    # Handle specific case of fractional onsets
                    onset_key = str(element.offset.numerator
                        / element.offset.denominator)[:4]
                else:
                    onset_key = str(element.offset)[:4] # Round up to 2 decimals

                if isinstance(element, m21.note.Note):
                    if element.duration.isGrace:
                        continue # Skip grace notes
                    elif onset_key in self.slice_dict:
                        # WARNING: the "offset" is the time of the beginning
                        # of the notes/the onset
                        # New offset time: not already encountered
                        self.slice_dict[onset_key].append(
                            element.pitch.midi)
                    else:
                        # Offset time already known
                        self.slice_dict[onset_key] = [element.pitch.midi]
                elif isinstance(element, m21.chord.Chord):
                    for note in element.notes:
                        if note.duration.isGrace:
                            continue # Skip grace notes
                        elif onset_key in self.slice_dict:
                            # New onset time: not already encountered
                            self.slice_dict[onset_key].append(
                                note.pitch.midi)
                        else:
                            # Offset time already known
                            self.slice_dict[onset_key] = [note.pitch.midi]
                elif isinstance(element, m21.note.Rest):
                    if onset_key not in self.slice_dict:
                        # Offset time already known
                        self.slice_dict[onset_key] = [] # Initialize empty list
        elif type(stream) == list: # List of notes dictionaries
            # Build events (note on or note off)
            event_list = []
            for note in stream:
                # Convert onset and duration values in number of quarter notes
                duration_fraction = note['duration'].split('/')
                if len(duration_fraction) == 2:
                    note_quarter_duration = (
                        4*float(duration_fraction[0])
                        / float(duration_fraction[-1])
                    )
                elif len(duration_fraction) == 1:
                    note_quarter_duration = 4*float(duration_fraction[0])
                if note_quarter_duration < 10e-5:
                    # Skip grace notes (with duration 0.)
                    continue

                onset_fraction = note['onset'].split('/')
                if len(onset_fraction) == 2:
                    onset_quarter = (
                        4*float(onset_fraction[0])
                        / float(onset_fraction[-1])
                    )
                elif len(onset_fraction) == 1:
                    onset_quarter = 4*float(onset_fraction[0])
                
                # Save events
                event_list.append({ # event_note_on
                    'time': onset_quarter,
                    'midi_pitch': int(note['midi'])
                })
                event_list.append({ # event_note_off
                    'time': onset_quarter + note_quarter_duration,
                    'midi_pitch': int(note['midi'])
                })

            # Sort events chronologically
            event_list = sorted(event_list, key=lambda x:x['time'])

            # Build slices from events
            current_time_key = "0.0"
            self.slice_dict['0.0'] = []
            for event in event_list:
                time_key = str(event['time'])[:4]
                if time_key != current_time_key:
                    # New time, new slice
                    self.slice_dict[time_key] = list( # Copy of previous slice
                        self.slice_dict[current_time_key]
                    )
                    current_time_key = time_key

                # Update notes
                if event['midi_pitch'] in self.slice_dict[current_time_key]:
                    # Remove pitch (note_off)
                    self.slice_dict[current_time_key].remove(event['midi_pitch'])
                else:
                    # Add pitch (note_on)
                    self.slice_dict[current_time_key].append(event['midi_pitch'])
            # Remove last slice if corresponding to the end of the excerpt
            ending_time_key = str(self.length_quarter)[:4]
            if ending_time_key in self.slice_dict:
                del self.slice_dict[ending_time_key]
        else:
            raise ValueError("'stream' argument must be as music21 Stream or a list of note dictionaries.")

    def __repr__(self):
        return str(self.slice_dict)

    ## Secondary methods

    def length(self, unit):
        """
        Return the length of the original stream
        in given unit of time.

        Parameters
        ----------
        unit : str in {"quarter", "beat"}
            If "beat" is given, the length is returned in number of quarter notes.
            If "beat" is given, the length is returned in number of beats.
            Shorthands "q" and "b" can be used.

        Returns
        -------
        float
            Duration of the original stream.
        """
        if unit in ["quarter", "q"]:
            return self.length_quarter
        elif unit in ["beat", "b"]:
            return self.length_beat
        else:
            raise ValueError("'unit' argument must be 'quarter' or 'beat' (or shorthand 'q' or 'b')")

    def time_list(self, unit="quarter", to_float=True):
        """
        Return the list of offset times, the number of time unit
        between the beginning of slices and the beginning of the
        stream, in chronological order.

        Notes
        -----
        Warning: while the term "onset" refers to the beginning of a note,
        the term "offset" is not its opposite, here, but the position of
        the object in the stream, as used in music21 (http://web.mit.edu/music21/doc/moduleReference/moduleBase.html#music21.base.Music21Object.offset).
        In other words, the "offset" of a note, is the moment when this note
        is played, expressed in quarter length, since the beginning of the
        extract.

        Parameters
        ----------
        unit : str in {"quarter", "beat", "q", "b"}
            If "beat" is given, time values are given in number of quarter notes.
            If "beat" is given, time values are given in number of beats.
            Shorthands "q" and "b" can be used.
            Defaults to "quarter".
        to_float : bool
            If True, the elements of the returned list are given as float. Else,
            the values are kept as strings. Defaults to True.
        
        Returns
        -------
        list of times of slice beginning
        """
        if to_float:
            slice_times = sorted([float(time) for time in self.slice_dict.keys()])
        else:
            slice_times = sorted(list(self.slice_dict.keys()))

        if unit in ["quarter", "q"]:
            return slice_times
        elif unit in ["beat", "b"]:
            return [slice_time/self.beat_duration for slice_time in slice_times]
        else:
            raise ValueError("'unit' argument must be 'quarter' or 'beat' (or shorthand 'q' or 'b')")

    def duration_list(self, unit="quarter"):
        """
        Return the list of duration of each successive slice.

        Parameters
        ----------
        unit : str in {"quarter", "beat"}
            If "beat" is given, time values are given in number of quarter notes.
            If "beat" is given, time values are given in number of beats.
            Shorthands "q" and "b" can be used.
            Defaults to "quarter".
        
        Returns
        -------
        list of float
        """
        if not self.slice_dict:
            # Empty measure
            return [self.length(unit=unit)]

        times = self.time_list(unit=unit)
        times.append(self.length(unit=unit))
        durations = []
        for i in range(1, len(times)):
            durations.append(times[i]-times[i-1])
        return durations

    def chord_list(self):
        """
        Return the list of all the groups of pitches heard in the slices.

        Returns
        -------
        list of (list of int)
            Pitches are given in MIDI: 60 for C4.
        """
        return list(self.slice_dict.values())

    def weighted_chord_list(self, unit="quarter"):
        """
        Return the list of all the groups of pitches heard in the slices,
        weigthed by the duration of the slice/temporal segment.

        Parameters
        ----------
        unit : str in {"quarter", "beat"}
            If "beat" is given, time values are given in number of quarter notes.
            If "beat" is given, time values are given in number of beats.
            Shorthands "q" and "b" can be used.
            Defaults to "quarter".
        """
        durations = self.duration_list(unit=unit)
        weighted_list = []
        for i, (time, chord) in enumerate(sorted(self.slice_dict.items())):
            weighted_list.append((chord, durations[i]))
        return weighted_list

    ## Descriptors

    def n_slices(self, norm_by_length=True):
        """
        Return the total number of slices in the stream,
        divided by the length of the stream in number of beats.

        Returns
        -------
        float
        """
        n_slices = len(self.slice_dict)
        if norm_by_length:
            n_slices /= self.length_beat
        return n_slices

    def has_even_meter(self):
        """
        Return True if the time signature corresponds to
        an even number of beats per measure, and False otherwise.

        Returns
        -------
        bool
        """
        return self.length_beat % 2 == 0

    def prop_silence(self):
        """
        Returns the proportion of the overall duration of the stream that is
        complete silence.

        Returns
        -------
        float
        """
        silence_length = 0.
        for chord, duration in self.weighted_chord_list(unit='quarter'):
            if not chord:
                # Found silence
                silence_length += duration
        return silence_length / self.length_quarter

    def longest_silence(self):
        """
        Returns the proportion of the longest slice of silence
        over the overall duration of the stream.

        Returns
        -------
        float
        """
        max_silence_length = 0.
        for chord, duration in self.weighted_chord_list(unit='quarter'):
            if not chord and duration > max_silence_length:
                # Found longest silence
                max_silence_length += duration
        return max_silence_length / self.length_quarter

    def stats_harmonicity(self):
        """
        Return statistics on the harmonicity over the stream.

        For a given slice or segment, its value of harmonicity corresponds 
        to the proportion of 'consonant' intervals in all the pairs of 
        simultaneous pitch classes in this slice. Here, we define consonant 
        intervals as the ensemble of octaves, perfect fifth, thirds, their 
        inversions (sixths and perfect fourth). The returned harmonicity 
        values are statistics on the ensemble of harmonicity value of all 
        the slices of the stream, weighted by their duration.

        Returns
        -------
        float
            Average harmonicity over time, computed over slices and weighted by their duration.
        float
            Standard deviation of harmonicity in the stream.
        float
            Minimal value of harmonicity over the slices.
        float
            Maximal value of harmonicity over the slices.
        float
            Weighted median harmonicity over time.
        """
        weighted_harmonicities = []
        for chord, duration in self.weighted_chord_list(unit='quarter'):
            len_chord = len(chord)
            n_combinations = len_chord*(len_chord-1) / 2 # Number of distinct pair of pitches
            if len_chord <= 1:
                weighted_harmonicities.append((1., duration))
            else:
                current_harmonicity = 0
                for pitch1 in range(len_chord):
                    for pitch2 in range(1+pitch1, len_chord):
                        interval = abs(chord[pitch1]-chord[pitch2]) % 12
                        if interval in [0, 3, 4, 5, 7, 8, 9]:
                            # Authorized intervals are thirds, sixths,
                            # perfect fourths and fifths, and octaves
                            current_harmonicity += 1
                # Normalize by the number of possible harmonic intervals
                current_harmonicity /= n_combinations
                weighted_harmonicities.append(( # Tuple of ...
                    current_harmonicity, # Harmonicity of the slice
                    duration # Weight
                ))
        return weighted_stats(weighted_harmonicities)

    def stats_width(self):
        """
        Return statistics on the width of slices in the stream.

        The width is defined here as the space between the lower and the
        highest pitches that are heard simultaneously.
        Silent slices have a width of 0; single pitches correspond to a
        width of value 1; and for more pitches, the width is computed as
        1 plus the number of semitones between the extremal pitches in the
        slice.

        Returns
        -------
        float
            Average width over time, computed over slices and weighted by their duration.
        float
            Standard deviation of width in the stream.
        int
            Minimal width over the slices.
        int
            Maximal width over the slices.
        number (int or float)
            Weighted median width.
        """
        weighted_widths = []
        for chord, duration in self.weighted_chord_list(unit='quarter'):
            if not chord:
                width = 0 # Not notes, no width
            elif len(chord) == 1:
                width = 1 # Single pitch, unitary width
            else:
                width = max(chord) - min(chord) + 1
            weighted_widths.append((width, duration))
        return weighted_stats(weighted_widths)

    def stats_n_voices(self):
        """
        Return statistics on the number of voices over time.

        We call number of voices the number of threads or distinct
        pitches played simultaneously in a slice.

        Returns
        -------
        float
            Average number of voices over time.
        float
            Standard deviation of the number of voices.
        int
            Minimal number of voices in the slices.
        int
            Maximal number of voices in the slices.
        number (int or float)
            Weighted median number of voices.
        """
        weighted_n_voices = [
            (len(chord), duration)
            for chord, duration
            in self.weighted_chord_list(unit='quarter')
        ]
        return weighted_stats(weighted_n_voices)

    def stats_n_gaps(self):
        """
        Return statistics on the number of gaps in simultaneously heard
        pitches in the stream.

        The "gaps" are the harmonic intervals, between successive notes
        played simultaneously, that are strictly wider than a perfect fourth
        (6 semitones or more), as defined by Q. R. Nordgren.

        Returns
        -------
        float
            Average number of gaps over time.
        float
            Standard deviation of the number of gaps.
        int
            Minimal number of gaps in the slices.
        int
            Maximal number of gaps in the slices.
        number (int or float)
            Weighted median number of gaps.

        References
        ----------
        Q. R. Nordgren. A measure of textural patterns and strengths. J. of Music Theory, 4(1):19–31, Apr. 1960
        """
        weighted_n_gaps = []
        for chord, duration in self.weighted_chord_list(unit='quarter'):
            n_gaps = 0
            all_pitches = sorted(chord)
            for i in range(len(chord)-1):
                if all_pitches[i+1]-all_pitches[i] > 5: # 5 semitones = fourth
                    # Gap detected
                    n_gaps += 1
            weighted_n_gaps.append((n_gaps, duration))
        return weighted_stats(weighted_n_gaps)

    def melodic_intervals(self, interval, onset_dict):
        """
        Return the importance of given interval in consecutive slices, 
        normalized by the number of notes in studied section. 
        We detect one melodic interval for each note in an onset, only
        if that notes exists as transposed by target interval, in the
        neighbouring previous slice.

        Parameters
        ----------
        interval : str
            Target melodic interval, in {'second', 'thirds',
            'fourths-fifths', 'octaves', 'unison'}

        Returns
        -------
        float

        Note
        ----
        This method requires information from both Slices and Onsets 
        structures. From the onsets, we know if the notes are new notes
        (and not simply  sustained from previous slices); with slices, 
        we make sure that the previous note remains until the next 
        one. 
        """
        interval_dict = { # complete name, / shorthand
            'seconds': [1, -1, 2, -2], 's': [1, -1, 2, -2],
            'thirds': [3, -3, 4, -4], 't': [3, -3, 4, -4],
            'fourths-fifths': [5, -5, 7, -7], 'f': [5, -5, 7, -7],
            'octaves': [12, -12], 'o': [12, -12],
            'unison': [0], 'u': [0]
        }
        if interval not in interval_dict:
            raise ValueError("Argument 'interval' must be in", list(interval_dict.keys()))
        interval_semitones = interval_dict[interval]
        slice_time_list = self.time_list(unit='quarter', to_float=False)

        number_of_notes = 0
        detected_interval = 0
        for onset_time, pitches in onset_dict.items():
            # Get the pitches in slice just before current onset:
            # the pitches heard before new pitches are played
            previous_slice_index = slice_time_list.index(onset_time) - 1
            if previous_slice_index < 0:
                continue # Skip the first onset
            previous_pitches = self.slice_dict[
                slice_time_list[previous_slice_index]
            ]

            # Build pitches that fit current onset by moving by given interval
            possible_final_pitches = []
            for i in interval_semitones:
                possible_final_pitches += [p+i for p in previous_pitches]
            # Detect given melodic intervals from previous pitches
            for pitch in pitches:
                number_of_notes += 1
                if pitch in possible_final_pitches:
                    detected_interval += 1

        if not number_of_notes:
            return 0.
        return detected_interval / number_of_notes

#################
# Demo function #
#################

def display_all_descriptors(stream):
    """
    Compute and print all the available descriptors in given stream.

    Parameters
    ---------
    stream : music21.stream.Stream object or list of note dictionaries.

    Returns
    -------
    None
    """
    # Build structures
    pitches = Pitches(stream)
    onsets = Onsets(stream)
    slices = Slices(stream)

    # Display results
    print("## Structures")
    print("Pitches:", pitches)
    print("Onsets:", onsets)
    print("Slices:", slices)

    print("Duration of the measure (in quarter notes):",
        pitches.length_quarter)
    print("Duration of a beat:", pitches.beat_duration)
    print("Duration of the measure (in beats):", pitches.length_beat)

    print("\n## Unary descriptors")
    print("Has even meter:", slices.has_even_meter())
    print("Number of notes per beat:", pitches.n_notes())
    print("Number of pitches per beat:", pitches.n_pitches())
    print("Number of pitch classes per beat:", pitches.n_pitchclasses())
    print("Number of onsets per beat:", onsets.n_onsets())
    print("Number of slices per beat:", slices.n_slices())
    print("Proportion of new pitches", pitches.novelty())
    print("Proportion of silence:", slices.prop_silence())
    print("Proportion of the longest silence:", slices.longest_silence())

    print("\n## Composed descriptors")
    print(f"{'Descriptors':>26} | Avg   | Std   | Min   | Max   | Med")
    composed_descriptors = {
        "Pitch": pitches.stats_pitches(),
        "Duration": pitches.stats_durations(),
        "Regularity of onsets": onsets.stats_regularity(),
        "Width over time": slices.stats_width(),
        "Number of voices per onset": onsets.stats_n_voices(),
        "Number of voices over time": slices.stats_n_voices(),
        "Number of gaps per onset": onsets.stats_n_gaps(),
        "Number of gaps over time": slices.stats_n_gaps(),
        "Harmonicity over time": slices.stats_harmonicity()
    }
    for descriptor, values in composed_descriptors.items():
        avg, std, min_v, max_v, med = values
        print(
            f"{descriptor:>26} | "
            f"{str(avg)[:5]:<5} | "
            f"{str(std)[:5]:<5} | "
            f"{str(min_v)[:5]:<5} | "
            f"{str(max_v)[:5]:<5} | "
            f"{str(med)[:5]:<5}"
        )

    print("\n## Harmonic intervals")
    hi_t, hi_f, hi_o = onsets.harmonic_intervals()
    print("    Thirds or sixths:", hi_t)
    print("    Fourths or fifths:", hi_f)
    print("    Octaves:", hi_o)
    print("## Melodic intervals")
    print("    Seconds:",
        slices.melodic_intervals("s", onsets.onset_dict))
    print("    Thirds:",
        slices.melodic_intervals("t", onsets.onset_dict))
    print("    Fourths or fifths:",
        slices.melodic_intervals("f", onsets.onset_dict))
    print("    Octaves:",
        slices.melodic_intervals("o", onsets.onset_dict))
    print("    Repetition (unison):",
        slices.melodic_intervals("u", onsets.onset_dict))
