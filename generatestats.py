"""
This file is part of "Symbolic Texture Dataset" http://algomus.fr/code 
Copyright (C) 2021-2023 by Louis Couturier, 
louis.couturier@u-picardie.fr

"Symbolic Texture Dataset" is free software: you can redistribute it 
and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of 
the License, or (at your option) any later version.

"Symbolic Texture Dataset" is distributed in the hope that it will be 
useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
General Public License for more details.

You should have received a copy of the GNU General Public License 
along with "Symbolic Texture Dataset". If not, see
http://www.gnu.org/licenses/
"""

from collections import Counter
import csv
import os

import matplotlib.pyplot as plt
from matplotlib import cm, colors

from textureclass import Texture


####################
# Global variables #
####################

PIECES = [
    "K279-1",
    "K279-2",
    "K279-3",
    "K280-1",
    "K280-2",
    "K280-3",
    "K283-1",
    "K283-2",
    "K283-3",
]

ANNOTATION_DIRECTORY = os.path.join("dataset", "tsv")
OUTPUT_DIRECTORY = "stats"

TEXTURAL_ELEMENTS = [
    "M", "H", "S",
    "h", "p", "o", "h+", "p+",
    "s", "t", "b", "r",
    "_", ","
]


#############
# Functions #
#############

## Extracting meaningful data

def get_textures_from_tsv(
        pieces=PIECES,
        input_directory=ANNOTATION_DIRECTORY,
        high_level_only=False):
    """
    Return a list of texture label extracted from given pieces.

    Parameters
    ----------
    pieces: list of str
        List of the pieces, movements, files to be parsed.
    input_directory: str or Path object
        Path to the folder containing target pieces.
    high_level_only: bool
        If True, does not take account of sublayers.
        Defaults to False.

    Returns
    -------
    list of str
    """
    annotation_files = [
        os.path.join(input_directory, piece + ".tsv")
        for piece in pieces
    ]

    texture_list = []
    for annotation_file in annotation_files:
        with open(annotation_file) as tsv_file:
            data = csv.DictReader(tsv_file, delimiter="\t")
            for row in data:
                texture_list.append(
                    str(Texture(row["label"])) if not high_level_only
                    else Texture(row["label"]).high_level()
                )
                # The call of 'Texture' ensure that the format is
                # standardized and the syntax is consistent.
    
    return texture_list

def get_layers_from_tsv(pieces=PIECES,
        input_directory=ANNOTATION_DIRECTORY,
        high_level_only=False):
    """
    Return a list of all the textural layers extracted from given pieces

    Parameters
    ----------
    pieces: list of str
        List of the pieces, movements, files to be parsed
    input_directory: str or Path object
        Path to the folder containing target pieces
    high_level_only: bool
        If True, does not take account of sublayers.
        Defaults to False.

    Returns
    -------
    list of str
    """
    layer_list = []
    texture_list = get_textures_from_tsv(
        pieces=pieces,
        input_directory=input_directory,
        high_level_only=high_level_only
    )

    for texture_str in texture_list:
        for layer in Texture(texture_str).all_layers():
            layer_list.append(str(layer))

    return layer_list

def get_element_from_tsv(
        element,
        pieces=PIECES,
        available_elements=TEXTURAL_ELEMENTS,
        input_directory=ANNOTATION_DIRECTORY):
    """
    Return a list of boolean extracted from given pieces.

    Parameters
    ----------
    element: str
        Name of target textural element.
    available_elements: list of str
        List of the only accepted 'element' values.
    pieces: list of str
        List of the pieces, movements, files to be parsed.
    input_directory: str or Path object
        Path to the folder containing target pieces.

    Returns
    -------
    list of bool
    """
    annotation_files = [
        os.path.join(input_directory, piece + ".tsv")
        for piece in pieces
    ]

    if element not in available_elements:
        raise ValueError(
            "Invalid element argument. Given string must be in {}.".format(
                available_elements
            )
        )


    element_list = []
    for annotation_file in annotation_files:
        with open(annotation_file) as tsv_file:
            data = csv.DictReader(tsv_file, delimiter="\t")
            for row in data:
                element_list.append(
                    str(bool(int(row[element]))) # "1"->1->True; "0"->0->False
                )
    
    return element_list

def get_element_combination_from_tsv(
        elements=[],
        pieces=PIECES,
        available_elements=TEXTURAL_ELEMENTS,
        input_directory=ANNOTATION_DIRECTORY):
    """
    Return a list of element combination extracted from given pieces.

    Parameters
    ----------
    elements: list of str
        Names of target textural elements.
    available_elements: list of str
        List of the only accepted string values in 'elements' argument.
    pieces: list of str
        List of the pieces, movements, files to be parsed.
    input_directory: str or Path object
        Path to the folder containing target pieces.

    Returns
    -------
    list of str
    """
    annotation_files = [
        os.path.join(input_directory, piece + ".tsv")
        for piece in pieces
    ]

    for element in elements:
        if element not in available_elements:
            raise ValueError(
                "Invalid element argument. Given strings must be in {}.".format(
                    available_elements
                )
            )

    combination_list = []
    for annotation_file in annotation_files:
        with open(annotation_file) as tsv_file:
            data = csv.DictReader(tsv_file, delimiter="\t")
            for row in data:
                target_elements_found = []
                for element in elements:
                    if int(row[element]):
                        target_elements_found.append(element)
                combination_list.append(
                    ' '.join(sorted(target_elements_found))
                )

    return combination_list

def get_combinations_in_layers_from_tsv(
        attributes=[],
        pieces=PIECES,
        input_directory=ANNOTATION_DIRECTORY,
        high_level_only=False):
    """
    Return a list of all the textural layers extracted from given pieces

    Parameters
    ----------
    attributes: str
        All (single char) to look for in layers description
    pieces: list of str
        List of the pieces, movements, files to be parsed
    input_directory: str or Path object
        Path to the folder containing target pieces
    high_level_only: bool
        If True, does not take account of sublayers.
        Defaults to False.

    Returns
    -------
    list of str
    """
    texture_list = get_textures_from_tsv(
        pieces=pieces,
        input_directory=input_directory,
        high_level_only=high_level_only
    )

    combination_list = []
    for texture_str in texture_list:
        for layer in Texture(texture_str).all_layers():
            #layer_str = str(layer)
            combination = []
            for attribute in attributes:
                #if attribute in layer_str:
                if layer.has_element(
                        attribute,
                        high_level_only=high_level_only):
                    combination.append(attribute)
            combination_list.append(
                ' '.join(sorted(combination))
            )

    return combination_list

def get_diversity_density(
        mode=['diversity', 'density'],
        pieces=PIECES,
        input_directory=ANNOTATION_DIRECTORY):
    """
    Return a list of global density and/or diversity information
    extracted from given pieces. Extracted values concern each
    individual textural configuration: sequential textures (with
    ',') yield two values.

    Parameters
    ----------
    mode: list of str in {'diversity', 'density'}
        Only selected information will be collected.
    pieces: list of str
        List of the pieces, movements, files to be parsed.
    input_directory: str or Path object
        Path to the folder containing target pieces.

    Returns
    -------
    list of str
    """
    annotation_files = [
        os.path.join(input_directory, piece + ".tsv")
        for piece in pieces
    ]

    value_list = []
    for annotation_file in annotation_files:
        with open(annotation_file) as tsv_file:
            data = csv.DictReader(tsv_file, delimiter="\t")
            for row in data:
                texture = Texture(row["label"])
                values = []
                if texture.is_sequential():
                    next_values = []

                if 'diversity' in mode:
                    # Extract number of layers
                    values.append(str(len(texture)))
                    if texture.is_sequential():
                        next_values.append(str(len(texture.sequential)))
                if 'density' in mode:
                    # Extract global density
                    values.append(str(int(texture)))
                    if texture.is_sequential():
                        next_values.append(str(int(texture.sequential)))

                value_list.append('|'.join(values))
                if texture.is_sequential():
                    value_list.append('|'.join(next_values))

    return value_list


## Counting and saving

def top_elements_to_tsv(
        element_list,
        filename,
        output_directory=OUTPUT_DIRECTORY,
        n=None):
    """
    Generate a TSV file containing giving elements from given
    list, ranked by number of instances.

    Parameters
    ----------
    filename: str
        Name of the output file.
    output_directory: Path object or str
        Path to the folder in which storing output file.
    element_list: list
        List of element to count
    n: int
        Number of most common element to consider.
        Defaults to None: all the elements are taken.

    Returns
    -------
    None
    """
    counter = Counter(element_list)
    total_number = len(element_list)

    output_path = os.path.join(output_directory, filename)
    fieldnames = [
        os.path.basename(filename).split('.')[0],
        "#",
        "%"
    ]
    with open(output_path, 'w', newline='\n') as tsv_file:
        writer = csv.DictWriter(
            tsv_file,
            fieldnames=fieldnames,
            delimiter='\t'
        )
        writer.writeheader()
        for element, number in counter.most_common(n=n):
            writer.writerow({
                fieldnames[0]: element,
                fieldnames[1]: number,
                fieldnames[2]: round(100*number/total_number, 3)
            })

## Plot

def top_plot(
        filename=None,
        directory=OUTPUT_DIRECTORY,
        sort_label=False,
        display_values=True,
        n=20):
    if not filename:
        # Exit if no filename given
        return

    if ".tsv" not in filename:
        filename += ".tsv"
    filepath = os.path.join(directory, filename)

    with open(filepath) as tsv_file:
        data = csv.reader(tsv_file, delimiter="\t")
        row_list = [(row[0], row[1], row[2]) for row in data]

    row_list = row_list[1:] # Remove header
    if sort_label:
        row_list.sort(key=lambda e: e[0])
    else:
        row_list.sort(key=lambda e: float(e[2]), reverse=True)

    total = sum([int(row[1]) for row in row_list])
    if n: # Maximum number of element
        row_list = row_list[:n]

    element_names = [row[0] for row in row_list]
    values = [int(row[1]) for row in row_list]
    proportions = [float(row[2])/100 for row in row_list]

    fig, ax = plt.subplots()
    ax.barh(
        range(len(row_list), 0, -1),
        proportions,
        color='red',
        tick_label=element_names
    )

    if display_values:
        for i, proportion in enumerate(proportions):
            ax.text(
                proportion + 0.01,
                len(row_list) - i - 0.2,
                str(values[i]),
                color='black', fontsize=10
            )
        # Display total
        ax.text(
            max(proportions) * 0.9,
            len(row_list) + 0.8,
            "Total: " + str(total),
            color='black', fontsize=10
        )

    ax.set_xlim([0, max(proportions) * 1.1])
    ax.set_ylim([0, len(row_list) + 2.])
    ax.set_title(filename.replace(".tsv", ""))
    plt.tight_layout()
    plt.show()

def plot_textural_space(
        pieces=PIECES,
        input_directory=ANNOTATION_DIRECTORY):
    """
    Plot heatmap of density and diversity
    """
    density_values = [
        int(value)
        for value in get_diversity_density(
            mode=['density'],
            pieces=pieces,
            input_directory=input_directory
        )
    ]
    diversity_values = [
        int(value)
        for value in get_diversity_density(
            mode=['diversity'],
            pieces=pieces,
            input_directory=input_directory
        )
    ]

    n_samples = len(density_values)

    max_diversity = max(diversity_values)
    max_density = max(density_values)
    heatmap = [
        [0 for i in range(1, max_density+1)]
        for j in range(1, max_diversity+1)
    ]

    n_silence = 0
    for density, diversity in zip(density_values, diversity_values):
        if density*diversity == 0:
            n_silence += 1
            continue
        heatmap[diversity-1][density-1] += 1
    n_samples -= n_silence

    _, ax = plt.subplots()
    ax.imshow(
        heatmap,
        origin='lower',
        cmap=cm.Blues,
        norm=colors.LogNorm()
    )
    for i in range(max_density):
        for j in range(max_diversity):
            if heatmap[j][i]:
                ax.text(
                    i, j,
                    str(round(100*heatmap[j][i]/n_samples, 1))+"%",
                    ha='center', va='center',
                    color='darkgray' if (heatmap[j][i]/n_samples < 0.005) else 'white'
                )
    ax.set_xlabel("Number of voices (density)")
    ax.set_ylabel("Number of layers (diversity)")
    ax.set_xticks(list(range(max_density)))
    ax.set_xticklabels([str(k) for k in range(1, max_density+1)])
    ax.set_yticks(list(range(max_diversity)))
    ax.set_yticklabels([str(k) for k in range(1, max_diversity+1)])
    ax.set_title("Textural space")

    plt.show()



####################
## Main functions ##
####################

def generate_textures_stats(): # Main1
    ### TEXTURES ###
    # 1a: Most common textures
    top_elements_to_tsv(
        get_textures_from_tsv(),
        "top_textures.tsv"
    )

    # 1b: Most common texture, without considering sublayers
    top_elements_to_tsv(
        get_textures_from_tsv(high_level_only=True),
        "top_high_level_textures.tsv"
    )

def generate_layers_stats(): # Main 2
    ### LAYERS ###
    # 2a: Most common layers
    top_elements_to_tsv(
        get_layers_from_tsv(),
        "top_layers.tsv"
    )

    # 2b: Most common layers, without considering sublayers
    top_elements_to_tsv(
        get_layers_from_tsv(high_level_only=True),
        "top_high_level_layers.tsv"
    )

def generate_textural_elements_stats(): # Main 3
    ### TEXTURAL ELEMENTS ###
    global TEXTURAL_ELEMENTS
    ## Elements individually
    for i, textural_element in enumerate(TEXTURAL_ELEMENTS):
        top_elements_to_tsv(
            get_element_from_tsv(textural_element),
            "prop{}{}.tsv".format(
                i+1, # to avoid overwrinting H/h, S/s
                textural_element
            )
        )

    ## All combinations of textural elements (in global textures)
    top_elements_to_tsv(
        get_element_combination_from_tsv(
            elements=TEXTURAL_ELEMENTS
        ),
        "globalcomb.{}.tsv".format(
            ''.join(TEXTURAL_ELEMENTS)
        )
    )
    # " of M, H and S (in global texture)
    top_elements_to_tsv(
        get_element_combination_from_tsv(
            elements=["M", "H", "S"]
        ),
        "globalcomb.MHS.tsv"
    )
    # " of h, p, o (in global texture)
    top_elements_to_tsv(
        get_element_combination_from_tsv(
            elements=["h", "p", "o"]
        ),
        "globalcomb.hpo.tsv"
    )

def generate_elements_per_layer_stats():  # Main 4
    ## Combination of elements (in layers)
    # All combinations of textural elements (in layers)
    top_elements_to_tsv(
        get_combinations_in_layers_from_tsv(
            attributes=[
                "M", "H", "S",
                "1", "2", "3", "4", "5", "6", "7", "8",
                "h", "p", "o",
                "s", "t", "b", "r",
                "_"
            ]
        ),
        "layercomb.MHShpostbr_density.tsv"
    )
    # Density only (in layers)
    top_elements_to_tsv(
        get_combinations_in_layers_from_tsv(
            attributes=[
                "1", "2", "3", "4", "5", "6", "7", "8", "9"
            ],
            high_level_only=True
        ),
        "layerdensity.tsv"
    )
    # " of M, H and S (in layers)
    top_elements_to_tsv(
        get_combinations_in_layers_from_tsv(
            attributes=["M", "H", "S"],
            high_level_only=True
        ),
        "layercomb.MHS.tsv"
    )
    # " of M, H, S and density values (in layers)
    top_elements_to_tsv(
        get_combinations_in_layers_from_tsv(
            attributes=[
                "M", "H", "S",
                "1", "2", "3", "4", "5", "6", "7", "8"
            ],
            high_level_only=True
        ),
        "layercomb.densityMHS.tsv"
    )
    # " of M, H, S and h, p, o (in layers)
    top_elements_to_tsv(
        get_combinations_in_layers_from_tsv(
            attributes=[
                "M", "H", "S", "h", "p", "o"
            ],
            high_level_only=True
        ),
        "layercomb.MHShpo.tsv"
    )


def generate_diversity_density_stats():  # Main 4
    ### DIVERSITY AND DENSITY OF FULL TEXTURES
    # Diversity only
    top_elements_to_tsv(
        get_diversity_density(
            mode=['diversity']
        ),
        "diversity.tsv"
    )

    # Global density only
    top_elements_to_tsv(
        get_diversity_density(
            mode=['density']
        ),
        "globaldensity.tsv"
    )

    # Both
    top_elements_to_tsv(
        get_diversity_density(
            mode=['diversity', 'density']
        ),
        "diversity_globaldensity.tsv"
    )



if __name__ == "__main__":
    generate_textures_stats()
    generate_layers_stats()
    generate_textural_elements_stats()
    generate_elements_per_layer_stats()
    generate_diversity_density_stats()
    plot_textural_space()

    top_plot(
        ## Uncomment chosen option to plot target generated statistics
        #"top_textures"
        #"top_high_level_textures"
        #"top_layers"
        #"top_high_level_layers"
        #"globalcomb.hpo"
        #"layercomb.densityMHS"
        #"diversity_globaldensity"
        #"diversity", sort_label=True
        #"globaldensity", sort_label=True
    )
