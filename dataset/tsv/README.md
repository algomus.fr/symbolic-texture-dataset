| Column | Type | Description |
|--------|------|-------------|
|   mn   | int  | Measure number, continuous count of complete measures as used in printed editions. Starts with 1 except for pieces beginning with a pickup measure, numbered as 0. (This definition originates from [Annotated Mozart Sonatas](https://github.com/DCMLab/mozart_piano_sonatas/blob/main/notes/README.md), Hentschel et al., 2021) |
| label  | str  | Complete label of texture annotation
|   M    | bool | Indicates the presence of *melodic function* in at least one layer of the textural configuration. |
|   H    | bool | Indicates the presence of *harmonic function* in at least one layer of the textural configuration. |
|   S    | bool | Indicates the presence of *static function* in at least one layer of the textural configuration. |
|   h    | bool | Indicates the presence of *strict homorhythmy* in at least one layer of the textural configuration. |
|   p    | bool | Indicates the presence of *strictly parallel motions* in at least one layer of the textural configuration. |
|   o    | bool | Indicates the presence of *octave motions* in at least one layer of the textural configuration. |
|   h+   | bool | Indicates the presence of *homorhythmy* (including strict homorhythmy, strictly parallel motions or octave motions) in at least one layer of the textural configuration. |
|   p+   | bool | Indicates the presence of *parallel motions* (including strictly parallel motions or octave motions) in at least one layer of the textural configuration. |
|   s    | bool | Indicates the presence of *scale figure(s)* in at least one layer of the textural configuration. |
|   t    | bool | Indicates the presence of *sustained notes figure* in at least one layer of the textural configuration. |
|   b    | bool | Indicates the presence of *oscillation* ("battement"/"batterie") in at least one layer of the textural configuration. |
|   r    | bool | Indicates the presence of *consecutively repeated notes or chords* in at least one layer of the textural configuration. |
|   _    | bool | Indicates the presence of *sparsity* (low horizontal density, or silence) in at least one layer of the textural configuration. |
|   ,    | bool | Indicates the presence of *sequential separation* in label: the annotated measure can be cut in half around its center to separate two distinct textural configurations. |