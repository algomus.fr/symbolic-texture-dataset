# W. A. Mozart, Piano Sonata No. 5
# K283 / K189h
# mvmt. 3, Presto

# tonality: G major
# time-signature: 3/8
# annotator: Louis Couturier
# reviewer: Louis Bigo

% Exposition
%% First subject
 * 1 : M2p/S1r
 * 2 : M2p/S1r
 * 3 : M2p/S1r
 * 4 : M2p/S1r
 * 5 : M2p/S1r
 * 6 : M2p/S1r
 * 7 : M2p/S1r
 * 8 : M2p/S1r
 * 9 : M1s/M2ot
 * 10 : M1s/M2ot
 * 11 : M1s/M2ot
 * 12 : M1s/M2ot
 * 13 : M2h/HS1(S1/M1)
 * 14 : M2p/HS1(S1/M1)
 * 15 : M2h/HS1(S1/M1)
 * 16 : M2h/HS1(S1/M1)
 * 17 : 1[M1s/H2h_]
 * 18 : 1[M1/MH2h_]
 * 19 : M1t/MH2p
 * 20 : M1t/MH2p
 * 21 : M1t/MH2h
 * 22 : M1t/MH2h
 * 23 : M1t/MH2h
 * 24 : 1[M1/MH2h_]
%% Transition
 * 25 : MH3ht/S1
 * 26 : 1[MH1/S1_]
 * 27 : MH3ht/S1
 * 28 : 1[MH1/S1_]
 * 29 : MH3ht/S1
 * 30 : 1[MH1/S1_]
 * 31 : MH3ht/S1
 * 32 : 1[M1/S1_]
 * 33 : M1/HS1(S1/M1)
 * 34 : M1/HS1(S1/M1)
 * 35 : M1/HS1(S1/M1)
 * 36 : M1/HS1(S1/M1)
 * 37 : M1/HS1(S1/M1)
 * 38 : M1/HS1(S1/M1)
 * 39 : MHS4h(H3hr/S1)
 * 40 : HS4h(H3h/S1), _
%% Second subject
 * 41 : MS1
 * 42 : MS1
 * 43 : M2p/S2ot
 * 44 : M2p/S2ot
 * 45 : M2h/S2o, M1/M1
 * 46 : M1/M1
 * 47 : M1/M1
 * 48 : M1/M1, M1s
 * 49 : MS1
 * 50 : MS1
 * 51 : M2p/S2ot
 * 52 : M2p/S2ot
 * 53 : M2p/S2ot, M1/M1
 * 54 : M1/M1
 * 55 : M1/M1
 * 56 : M1/M1, M2p(M1s/M1)
 * 57 : 3[M2p_/MS2o]
 * 58 : MS2o, M2p(M1s/M1)
 * 59 : 3[MS2o/M2h_]
 * 60 : MS2o, M2p(M1s/M1)
 * 61 : 3[M2h_/MS2o]
 * 62 : MS2o, M2p(M1s/M1)
 * 63 : 3[MS2o/M2h_]
 * 64 : MS2o, M1s
 * 65 : 2[MHS1/HS3h_]
 * 66 : 2[MHS1/HS3h_]
 * 67 : 2[MHS1/HS3h_]
 * 68 : 2[MHS1/HS3h_]
 * 69 : 2[H3h_/MH1]
 * 70 : 2[H3h_/MH1]
 * 71 : M1t/HS1(S1/M1)
 * 72 : M1t/HS1(S1/M1)
 * 73 : MH3h(M1/H2h), M1
 * 74 : 1[M1/H2h_]
 * 75 : 2[M1/H2h_]
 * 76 : 1[M1/H2h_]
 * 77 : M1/H2h
 * 78 : M1/H2h
 * 79 : 3h[M1/H2p]
 * 80 : M1t/M1/M1
 * 81 : 1[M1_/MS1]
 * 82 : 1[H2h_/M1]
 * 83 : 2[H2h_/M1]
 * 84 : 1[H2h_/M1]
 * 85 : H2h/M1
 * 86 : H2h/M1
 * 87 : MH3h(H2h/M1), _
 * 88 : MH3h(H2h/M1), _
 * 89 : 2[MH1/H3h_]
 * 90 : 2[MH1/H3h_]
 * 91 : 2[H3h_/MH1]
 * 92 : 2[H3h_/MH1]
 * 93 : HS1b/M2o
 * 94 : HS1b/M2o
 * 95 : HS1b/M2o
 * 96 : HS1b/M2o
 * 97 : MH4h(H2h/M2o), M2o
 * 98 : M2o
 * 99 : M2o
 * 100 : M2o
 * 101 : M2o, MH5h(H3h/M2o)
 * 102 : MH6h(H4h/M2o), _
% Development
 * 103 : 2[M1r/HS3h_]
 * 104 : M1/HS3hr
 * 105 : 2[M1r/HS3h_]
 * 106 : M1/HS3hr
 * 107 : MH1s/H4ht
 * 108 : MH1s/H4ht
 * 109 : MH1s/H4ht
 * 110 : 1[MH1s/H4h_]
 * 111 : 2[M1r/HS3h_]
 * 112 : M1/HS3hr
 * 113 : 2[M1r/HS3h_]
 * 114 : M1/HS3hr
 * 115 : 1[MH1s/H4h_]
 * 116 : MH1s
 * 117 : 1[MH1s/H3h_]
 * 118 : MH1s
 * 119 : MH1s/H4ht
 * 120 : MH1s/H4ht
 * 121 : 1[MH1s/H4h_]
 * 122 : M1
 * 123 : M1/S1b
 * 124 : M2p/S1b
 * 125 : M2p/S1b
 * 126 : M2p/S1b
 * 127 : S1/M2h
 * 128 : S1/M2h
 * 129 : S1/M2h
 * 130 : S1/M2h
 * 131 : 2[S1/M2h_]
 * 132 : 2[MH1/H3h_]
 * 133 : 2[MH1/H3h_]
 * 134 : 2[MH1/H3h_]
 * 135 : 2[MH1/H3h_]
 * 136 : M1t/HS1(S1/M1)
 * 137 : M1t/HS1(S1/M1)
 * 138 : H3h(M1/H2h), M2o
%% Concluding bars of the exposition
 * 139 : M2o
 * 140 : M2o
 * 141 : M2o
 * 142 : M2o, MH5h(H3h/M2o)
 * 143 : MH5h(H3h/M2o), M2o
 * 144 : M2o, MH5h(H3h/M2o)
 * 145 : MH6h(H4h/M2o), M2o
 * 146 : M2o, MH5h(H3h/M2o)
 * 147 : MH6h(H4h/M2o), M1
%% Episodical passage leading to Recapitulation
 * 148 : M1
 * 149 : M1
 * 150 : M1t/HS3hr
 * 151 : M1/HS3hr
 * 152 : 3[M1/HS3h_]
 * 153 : M1
 * 154 : M1t/HS2hr
 * 155 : 3[M1_/HS3hr]
 * 156 : M1
 * 157 : M1
 * 158 : M2h/M1t
 * 159 : 3[M2p/M1_]
 * 160 : M2p/M1, MS1b
 * 161 : MS1b
 * 162 : MH2t/MS1b
 * 163 : MH2t/MS1b
 * 164 : MH2h/MS1, MS1b
 * 165 : MS1b
 * 166 : M1/HS2pb
 * 167 : M1t/HS2pb
 * 168 : 1[M1s/H2h_]
 * 169 : M1s
 * 170 : M1, _
 * 171 : _
% Recapitulation
%% First subject
 * 172 : M2p/S1r
 * 173 : M2p/S1r
 * 174 : M2p/S1r
 * 175 : M2p/S1r
 * 176 : M2p/S1r
 * 177 : M2p/S1r
 * 178 : M2p/S1r
 * 179 : M2p/S1r
 * 180 : M1s/M2ot
 * 181 : M1s/M2ot
 * 182 : M1s/M2ot
 * 183 : M1s/M2ot
 * 184 : M2h/HS1(S1/M1)
 * 185 : M2p/HS1(S1/M1)
 * 186 : M2h/HS1(S1/M1)
 * 187 : M2h/HS1(S1/M1)
 * 188 : 1[M1s/H2h_]
 * 189 : 1[M1/H2h_]
 * 190 : M1t/MH2p
 * 191 : M1t/MH2p
 * 192 : M1t/MH2h
 * 193 : M1t/MH2h
 * 194 : M1t/MH2h
 * 195 : 1[M1/MH2h_]
%% Transition
 * 196 : MH3ht/S1
 * 197 : 1[MH1/S1_]
 * 198 : MH3ht/S1
 * 199 : 1[MH1/S1_]
 * 200 : MH3ht/S1
 * 201 : 1[MH1/S1_]
 * 202 : MH3ht/S1
 * 203 : 1[M1/S1_]
 * 204 : M1/HS1(S1/M1)
 * 205 : M1/HS1(S1/M1)
 * 206 : M1/HS1(S1/M1)
 * 207 : M1/HS1(S1/M1)
 * 208 : M1/HS1(S1/M1)
 * 209 : M1/HS1(S1/M1)
 * 210 : MHS4h(H3hr/MH1)
 * 211 : HS4h, _
%% Second subject
 * 212 : MS1
 * 213 : MS1
 * 214 : M2p/S2ot
 * 215 : M2p/S2ot
 * 216 : M2p/S2ot, M1/M1
 * 217 : M1/M1
 * 218 : M1/M1
 * 219 : M1/M1, M1s
 * 220 : MS1
 * 221 : MS1
 * 222 : M2p/S2ot
 * 223 : M2p/S2ot
 * 224 : M2p/S2ot, M1/M1
 * 225 : M1/M1
 * 226 : M1/M1
 * 227 : M1/M1, M2p(M1s/M1)
 * 228 : 3[M2p_/MS2o]
 * 229 : MS2o, M2p(M1s/M1)
 * 230 : 3[MS2o/M2h_]
 * 231 : MS2o, M2p(M1s/M1)
 * 232 : 3[M2h_/MS2o]
 * 233 : MS2o, M2p(M1s/M1)
 * 234 : 3[MS2o/M2h_]
 * 235 : MS2o, M1
 * 236 : 2[MHS1/H3h_]
 * 237 : 2[MHS1/H3h_]
 * 238 : 2[MHS1/H3h_]
 * 239 : 2[MHS1/H3h_]
 * 240 : 2[H3h_/MH1]
 * 241 : 2[H3h_/MH1]
 * 242 : M1t/HS1(S1/M1)
 * 243 : M1t/HS1(S1/M1)
 * 244 : MH3h(M1/H2h), M1
 * 245 : 1[M1/H2h_]
 * 246 : 2[M1/H2h_]
 * 247 : 1[M1/H2h_]
 * 248 : M1/H2h
 * 249 : M1/H2h
 * 250 : 3h[M1/H2p]
 * 251 : M1t/M1/M1
 * 252 : 1[M1_/MS1]
 * 253 : 1[H2h_/M1]
 * 254 : 2[H2h_/M1]
 * 255 : 1[H2h_/M1]
 * 256 : H2h/M1
 * 257 : H2h/M1
 * 258 : MH3h(H2h/M1), _
 * 259 : MH3h(H2h/M1), _
 * 260 : 2[MH1/H3h_]
 * 261 : 2[MH1/H3h_]
 * 262 : 2[H3h_/MH1]
 * 263 : 2[H4h_/MH1]
 * 264 : HS3hb(HS1b/HS2hr)/M2o
 * 265 : HS3hb(HS1b/HS2hr)/M2o
 * 266 : HS3hb(HS1b/HS2hr)/M2o
 * 267 : HS3hb(HS1b/HS2hr)/M2o
 * 268 : H5h(H3h/M2o), M2o
 * 269 : M2o
 * 270 : M2o
 * 271 : M2o
 * 272 : M2o, H6h(H4h/M2o)
 * 273 : H5h(H3h/M2o), _
% CODA
 * 274 : MH4h(H4h/H4h), _
 * 275 : _
 * 276 : MH4h(H4h/H4h), _
 * 277 : _
