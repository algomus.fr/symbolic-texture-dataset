# W. A. Mozart, Piano Sonata No. 1
# K279 / K189d
# mvmt. 1, Allegro

# tonality: C major
# time-signature: 4/4
# annotator: Louis Couturier
# reviewer: Florence Levé

% Exposition
%% First subject
 * 1 : H3h/S1, M1s
 * 2 : H6h, M1/M1
 * 3 : H3h/S1, M1s
 * 4 : H6h, M1/M1
%% Transition
 * 5 : M1/HS1(S1/M1)
 * 6 : M1/HS1(S1/M1)
 * 7 : M1/HS1(S1/M1)
 * 8 : M1/HS1(S1/M1)
 * 9 : H1
 * 10 : M1/MH2h
 * 11 : H1
 * 12 : M1/MH2h, MH3h/S1
 * 13 : MH2p/S1
 * 14 : MH2p/S1_, M1s/MS1(M1/S1)
 * 15 : 2[M1s/MS1_(M1/S1)]
 * 16 : MH3(M2p/S1), MS1
%% Second subject
 * 17 : 1[M1/H1]
 * 18 : 1[MS1/HS2o_]
 * 19 : 1[M1/H1]
 * 20 : M1/HS1(S1/M1)
 * 21 : M1/HS1(S1/M1)
 * 22 : M1s/M1
 * 23 : M1s/M1
 * 24 : M1s/M1
 * 25 : M1s/M1, M1/HS1(S1/M1)
 * 26 : M1/HS1(S1/M1)
 * 27 : M1/HS1(S1/M1), 2[M1/H2p]
 * 28 : 2[M1/H2p]
 * 29 : MH1s
 * 30 : M1/HS1(S1/M1)
 * 31 : MHS1s/HS1(S1/M1)
 * 32 : MHS1s/HS1(S1/M1), M1s/H3h
 * 33 : MHS1s/S1
 * 34 : 3[MHS1s/H3h_/S1_]
 * 35 : 1[M1s/H2h_]
 * 36 : M1/M2o
 * 37 : M2p/S1, MS1s
 * 38 : MS1s, _
% Development
 * 39 : H3h/S1, MH1s
 * 40 : H3h/S1, MH1s
 * 41 : H3h/S1, MH1s
 * 42 : H3h/S1, MH1s
 * 43 : H3h/S1, MH1s
 * 44 : H3h/S1, MH1s
 * 45 : MS1s(S1/M1s)/M2o, M1/MS1s(S1/M1s)
 * 46 : MS1s(S1/M1s)/M1, M1/MS1s(S1/M1s)
 * 47 : MS1s(S1/M1s), M1
 * 48 : M1/HS1(S1/M1)
 * 49 : M1/HS1(S1/M1)
 * 50 : M1/HS1(S1/M1)
 * 51 : M1/HS1(S1/M1), M1s/S2o_
 * 52 : MH2h/S1, M1s/S1_
 * 53 : MH2h/S1, M1s/S1_
 * 54 : MH2h/S1, M1s/S1_
 * 55 : MH2h/S1, M1/S1_
 * 56 : MS2o, M1s
 * 57 : M1s
% Recapitulation
%% First subject
 * 58 : H3h/S1, M1
 * 59 : H6h, M1/M1
 * 60 : H3h/S1, M1s
 * 61 : H6h, M1/M1
%% Transition
 * 62 : M1/HS1(S1/M1)
 * 63 : M1/HS1(S1/M1)
 * 64 : M1/HS1(S1/M1)
 * 65 : M1/HS1(S1/M1)
 * 66 : M1/HS1(S1/M1)
 * 67 : M1/HS1(S1/M1), 3[MH3h/S1]
 * 68 : M2p/S1
 * 69 : 2[MH2p/S1_], _
%% Second subject
 * 70 : MS1, M1t/H1
 * 71 : M1s/S2
 * 72 : 1[MS1/S2o_]
 * 73 : 1[M1/H1]
 * 74 : M1s/H2h, M1/HS1(S1/M1)
 * 75 : M1/HS1(S1/M1)
 * 76 : M1/HS1(S1/M1), M1s/M1
 * 77 : M1s/M1
 * 78 : M1s/M1, M1/S1
 * 79 : M1/HS1(S1/M1)
 * 80 : M1/HS1(S1/M1), M1s/M1
 * 81 : M1s/M1
 * 82 : H1
 * 83 : H1
 * 84 : H1, M1s/MS1(M1/S1)
 * 85 : 2[M1s/MS1_(M1/S1)]
 * 86 : MH3(M2p/S1), M1/HS1(S1/M1)
 * 87 : M1/HS1(S1/M1)
 * 88 : M1/HS1(S1/M1), 2[M1/H2p]
 * 89 : 2[M1/H2p]
 * 90 : MH1
 * 91 : M1/HS1(S1/M1)
 * 92 : MHS1s/HS1(S1/M1)
 * 93 : MHS1s/HS1(S1/M1), M1s/H3h
 * 94 : MS1s/S1
 * 95 : MS1s/S1, M1s/H3h
 * 96 : 1[M1s/H2h_]
 * 97 : M1/M2o
 * 98 : MH2h/S1, MS1s
 * 99 : MHS1s
 * 100 : HS3hr/MH1, _
