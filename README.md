![Version 1.1](https://img.shields.io/badge/release-v1.1-blue)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7316712.svg)](https://doi.org/10.5281/zenodo.7316712)
[![GNU GPL v3 license](https://img.shields.io/badge/license-GPLv3-green)](http://www.gnu.org/licenses/)
[![Open Database v1.0 license](https://img.shields.io/badge/license-ODbL-green)](https://opendatacommons.org/licenses/odbl/1-0/)

# A Dataset of Symbolic Texture Annotations in Mozart Piano Sonatas

Corpus ID: `corpus:MIR:mozartpianosonatas:texture:2023:version1.1`

This repository contains data and tools detailed in `Louis Couturier, Louis Bigo, & Florence Leve, 2022. A Dataset of Symbolic Texture Annotations in Mozart Piano Sonatas. Proceedings of the 23rd International Society for Music Information Retrieval Conference, 509–516.` https://doi.org/10.5281/zenodo.7316712


## Exploring the dataset

The `dataset/` folder contains textural annotations for 9 movements of Mozart piano sonatas 1, 2 and 5 (K279, K280, K283). The annotations follow a syntax dedicated to symbolic texture in piano music, defined in [this article](https://doi.org/10.5281/zenodo.6798318).

They are provided in three different formats:

<table>
<tr>
  <td> 

**Text**

  </td>
  <td>

- `.txt` files in the folder `dataset/txt/`.
- These files are more adapted to be human readable, eventhough they can be parsed easily.
- They contain metadata on the annotated movements, textural labels and structure information (all annotated movements are in Sonata Form)
- Files are structured as follows:
```
# <metadata>

% <structure_info>
%% <lower-level_structure_info>
 * <measure_number> : <texture_annotation>
...
```

  </td>
</tr>
<tr>
  <td> 

**TSV**  

  </td>
  <td>

- `.tsv` files in the folder `dataset/tsv/`.
- These files are more adapted to be parsed by modules and models.
- Tab-Separated Values files containing textural annotations, as well as all 14 textural elements for each label of texture.
- The meaning of each column is detailed in `dataset/tsv/README.md`

  </td>
</tr>
<tr>
  <td> 

**Dezrann**  

  </td>
  <td>

- `.dez` files in the folder `dataset/dez/`.
- These files can be read and updated on [Dezrann](dezrann.net), a web interface for the annotation of musical scores. This allows to visualize the score and the annotations simultaneously.
- Provided labels include textural annotations, macrostructure (Exposition, Coda etc.) and all the feedback labels used during the reviewing process (detailed in the article).

  </td>
</tr>
</table>

Annotations are also directly available online on Dezrann: for example, [K283.I](http://dezrann.net/~/mozart-piano-sonatas/k283.1).

## Dependencies

The code in this folder was implemented using Python3 (3.10.1). Please install following libraries to run it, for example using Pip (`pip install <library>`):
```
matplotlib=3.5.1
music21=7.3.1
```

If you intend to run models using `models.py` module, please install in addition:
```
pandas=1.4.2
scikit-learn=1.0.2
```

To generate the descriptors files, you will need to access the scores of the Mozart sonatas. You can clone Mozart Annotated Sonatas [repository](https://github.com/DCMLab/mozart_piano_sonatas) (Hentschel et al., 2021) in parent directory.


## Dig into the dataset

To generate relevant statistics, run the `generatestats` module. You can open a terminal in the top-level directory and launch:
```
python generatestats.py
```
It will save output files in the folder `stats/`.

You can also have a look in the `main` of this module to plot some of this data.


## Discover the descriptors

In this work, we defined and implemented 62 high-level descriptors of symbolic music. They are described in `descriptors/descriptors.py`.

To compute them on the dataset, run the `generatedescriptors` module.
```
python generatedescriptors.py
```
It will save output files in the folder `descriptors/`.

*Warning*: if path to encoded scores is erroneous, generated files will be empty.


## Predicting textural elements

Finally, we can use both descriptors (as input) and annotations (as ground truth data) to train machine learning models on texture prediction.

To launch textural elements prediction results using linear models, run the `models` module.
```
python models.py
```
- This might take about 30 seconds to run.
- Tested models are by default Logistic Regression models, Uniform random models and 'All True' models.
  - Support Vector Machine with linear kernel can also be tested by uncommenting the appropriate option in the file `models.py`.
- Results will be available in the generated file `linear_models.tsv`.


## Note on reproducibility

To reproduce the exact results presented in the [article](https://doi.org/10.5281/zenodo.7316712), you should use the [version 1.0](https://gitlab.com/algomus.fr/symbolic-texture-dataset/-/tags/v1.0) of this repository. The current version differs from the first one in the update of textural elements retrieval from full textural labels, resulting in updates of the TSV files (with pre-computed textural elements). But the full textural labels have not been altered in any manner.

Furthermore, the textural descriptors were extracted using the original release of the Annotated Mozart Sonatas (v1.0, Hentschel et al., [link](https://github.com/DCMLab/mozart_piano_sonatas/releases/tag/v1.0)).

## Authors and acknowledgement

Louis Couturier, Louis Bigo, Florence Levé from MIS, Université de Picardie Jules Verne, and CRIStAL, UMR 9189 CNRS, Université de Lille, France.

Contact: louis.couturier \[at\] u-picardie \[dot\] fr

Feel free to raise an issue if you encounter any unwanted behavior, or to share your suggestions.


## License

The code is licensed under the GNU General Public License v3.0 or any later version. The data (annotation files in `dataset`) are licensed under the Open Database License. Any rights in individual contents of the database are licensed under the Database Contents License.
