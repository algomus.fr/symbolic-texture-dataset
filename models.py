"""
This file is part of "Symbolic Texture Dataset" http://algomus.fr/code 
Copyright (C) 2021-2023 by Louis Couturier, 
louis.couturier@u-picardie.fr

"Symbolic Texture Dataset" is free software: you can redistribute it 
and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of 
the License, or (at your option) any later version.

"Symbolic Texture Dataset" is distributed in the hope that it will be 
useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
General Public License for more details.

You should have received a copy of the GNU General Public License 
along with "Symbolic Texture Dataset". If not, see
http://www.gnu.org/licenses/
"""

import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.dummy import DummyClassifier
from sklearn.svm import SVC, LinearSVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import (accuracy_score, confusion_matrix,
    f1_score, precision_score, recall_score)

from dataset import get_samples, PIECES
import warnings



#########
# Utils #
#########

def avg(data):
    """Return the average value of a list"""
    if not data:
        return None
    return sum(data) / len(data)

def std(data, avg_value=None):
    """Return the standard deviation of a list"""
    if not avg_value:
        avg_value = avg(data)
    return sum([abs(value-avg_value) for value in data]) / len(data)



##########
# Models #
##########



def cross_validation(textural_element, model='svm', verbose=2):
    """
    Parameters
    ----------
    textural_element : str
        label of the textural elements to predict
    verbose : int
        1 : print only final global train and test accuracies
        2 : print also individuals results
        3 : print data during execution
    """
    print("Prediction of", textural_element)
    global PIECES

    train_accuracies = []
    test_accuracies = []
    for piece in PIECES:
        validation_piece = [piece]
        train_pieces = [
            other_piece
            for other_piece in PIECES
            if other_piece != piece
        ]
        if model == 'lr':
            model = LogisticRegression() #class_weight='balanced')
        elif model == 'svm':
            model = SVC()
        elif model == 'dt':
            model = DecisionTreeClassifier(random_state=0, max_depth=3)
        x_train, y_train, identifiers_train = get_samples(
            textural_element, train_pieces
        )
        x_test, y_test, identifiers_test = get_samples(
            textural_element, validation_piece
        )

        model.fit(x_train, y_train)

        y_train_hat = model.predict(x_train)
        train_accuracy = accuracy_score(y_train, y_train_hat)*100
        y_test_hat = model.predict(x_test)
        test_accuracy = accuracy_score(y_test, y_test_hat)*100

        if verbose >= 3:
            print("Validation with", piece)
            print('Training confusion matrix:\n', confusion_matrix(y_train, y_train_hat))
            print('Training accuracy: %.4f %%' % train_accuracy)
            print('Testing confusion matrix:\n', confusion_matrix(y_test, y_test_hat))
            print('Testing accuracy: %.4f %%' % test_accuracy)
        train_accuracies.append(train_accuracy)
        test_accuracies.append(test_accuracy)

    if verbose >= 2:
        print("Summary:")
        for i, piece in enumerate(PIECES):
            print("  {}: (train) {:.3f}, (test) {:.3f}".format(
                piece, train_accuracies[i], test_accuracies[i]
            ))
    if verbose >= 1:
        print("Average training accuracy:", sum(train_accuracies)/len(train_accuracies))
        print("Average testing accuracy:", sum(test_accuracies)/len(test_accuracies))


def evaluate_models(
        textural_elements=['M'],
        models=[
            "LogReg",
            "SVM",
            "BaselineRandom",
            "BaselineAllTrue"
        ],
        ignorewarning=True,
        output="linear_models.tsv"):
    # Check arguments
    for m in models:
        if m not in [
                "LogReg",
                "SVM",
                "BaselineRandom",
                "BaselineAllTrue"
                ]:
            raise ValueError("Unknown model: {}".format(m))
    if ignorewarning:
        warnings.filterwarnings('ignore')

    data_dict = {
        "label": [],
        "model": [],
        "mean_validation_accuracy": [],
        "std_validation_accuracy": [],
        "mean_train_accuracy": [],
        "std_train_accuracy": [],
        "mean_f1_score": [],
        "std_f1_score": [],
        "mean_precision": [],
        "std_precision": [],
        "mean_recall": [],
        "std_recall": []
    }
    for textural_element in textural_elements:
        print("Textural element:", textural_element)
        for model_name in models:
            print("Model:", model_name)
            validation_accuracies = []
            train_accuracies = []
            precisions = []
            recalls = []
            f1_scores = []
        
            for piece in PIECES:
                print("Test set:", piece)
                # Build train and test set
                validation_piece = [piece]
                train_pieces = [
                    other_piece
                    for other_piece in PIECES
                    if other_piece != piece
                ]
                x_train, y_train, _ = get_samples(
                    textural_element, train_pieces
                )
                x_test, y_test, _ = get_samples(
                    textural_element, validation_piece
                )

                # Declare model
                # Note: in linear models, classes are balanced, weighted
                if model_name == "LogReg":
                    model = LogisticRegression(class_weight='balanced')
                elif model_name == "SVM":
                    # SVM with Linear kernel
                    model = LinearSVC(class_weight='balanced')
                elif model_name == "BaselineRandom":
                    # Baseline: dummy classifier, random
                    model = DummyClassifier(
                        strategy="uniform",
                        constant='1'
                    )
                elif model_name == "BaselineAllTrue":
                    # Baseline: dummy classifier, All true
                    model = DummyClassifier(
                        strategy="constant",
                        constant='1'
                    )
                else:
                    raise RuntimeError("Unknown model: {}".format(model_name))

                model.fit(x_train, y_train) # Train

                y_train_hat = model.predict(x_train)
                y_test_hat = model.predict(x_test)
                # Save results
                train_accuracies.append(accuracy_score(y_train, y_train_hat))
                validation_accuracies.append(accuracy_score(y_test, y_test_hat))
                #Debug: print(type(y_test), y_test, type(y_test_hat), y_test_hat)
                precisions.append(
                    precision_score(
                        y_test, y_test_hat,
                        pos_label='1' # Handle a bug in Sklearn ? (default value 1 not accepted)
                    )
                )
                recalls.append(
                    recall_score(
                        y_test, y_test_hat,
                        pos_label='1'
                    )
                )
                f1_scores.append(
                    f1_score(
                        y_test, y_test_hat,
                        pos_label='1'
                    )
                )
                print("  Test accuracy", validation_accuracies[-1])
                print("  Train accuracy", train_accuracies[-1])
                print("  F1 score", f1_scores[-1])
                print("  Precision", precisions[-1])
                print("  Recall", recalls[-1])

            # Assemble results
            data_dict["label"].append(textural_element)
            data_dict["model"].append(str(model))
            data_dict["mean_validation_accuracy"].append(
                avg(validation_accuracies))
            data_dict["mean_train_accuracy"].append(
                avg(train_accuracies))
            data_dict["mean_f1_score"].append(
                avg(f1_scores))
            data_dict["mean_precision"].append(
                avg(precisions))
            data_dict["mean_recall"].append(
                avg(recalls))
            data_dict["std_validation_accuracy"].append(
                std(validation_accuracies))
            data_dict["std_train_accuracy"].append(
                std(train_accuracies))
            data_dict["std_f1_score"].append(
                std(f1_scores))
            data_dict["std_precision"].append(
                std(precisions))
            data_dict["std_recall"].append(
                std(recalls))

    df = pd.DataFrame.from_dict(data_dict)
    df.to_csv(output, sep='\t')
    print("File saved !")



if __name__ == "__main__":
    # Test with Logistic Regression,
    # baseline with random uniform, and all-true

    evaluate_models(
        models=[
            "LogReg",
            #"SVM", #optional
            "BaselineRandom",
            "BaselineAllTrue"
        ],
        textural_elements=[
            'M', 'H', 'S', # functions
            'h', 'p', 'o', # relationships, parallelisms
            'h+', 'p+',
            's', 't', 'b', 'r', # figure
            '_', ','       # sparsity, sequentiality (separator)
        ]
    )
