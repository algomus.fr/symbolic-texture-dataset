"""
This file is part of "Symbolic Texture Dataset" http://algomus.fr/code 
Copyright (C) 2021-2023 by Louis Couturier, 
louis.couturier@u-picardie.fr

"Symbolic Texture Dataset" is free software: you can redistribute it 
and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of 
the License, or (at your option) any later version.

"Symbolic Texture Dataset" is distributed in the hope that it will be 
useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
General Public License for more details.

You should have received a copy of the GNU General Public License 
along with "Symbolic Texture Dataset". If not, see
http://www.gnu.org/licenses/
"""

import csv
import os

import numpy as np


####################
# Global variables #
####################

PIECES = [
    "K279-1",
    "K279-2",
    "K279-3",
    "K280-1",
    "K280-2",
    "K280-3",
    "K283-1",
    "K283-2",
    "K283-3",
]

DESCRIPTORS_DIRECTORY = os.path.join("descriptors")
GROUND_TRUTH_DIRECTORY = os.path.join("dataset", "tsv")

FEATURE_NAMES = [
    # Unary descriptors
    "has_even_meter",
    "n_notes",
    "n_pitches",
    "n_onsets",
    "n_slices",
    "n_pitchclasses",
    "novelty",
    "prop_silence",
    "longest_silence",
    # Composed descriptors
    "pitch_avg", "pitch_std",
    "pitch_min", "pitch_max",
    "pitch_med",
    "duration_avg", "duration_std",
    "duration_min", "duration_max",
    "duration_med",
    "regularity_avg", "regularity_std",
    "regularity_min", "regularity_max",
    "regularity_med",
    "n_voices_per_onset_avg", "n_voices_per_onset_std",
    "n_voices_per_onset_min", "n_voices_per_onset_max",
    "n_voices_per_onset_med",
    "n_gaps_per_onset_avg", "n_gaps_per_onset_std",
    "n_gaps_per_onset_min", "n_gaps_per_onset_max",
    "n_gaps_per_onset_med",
    "n_voices_per_beat_avg", "n_voices_per_beat_std",
    "n_voices_per_beat_min", "n_voices_per_beat_max",
    "n_voices_per_beat_med",
    "n_gaps_per_beat_avg", "n_gaps_per_beat_std",
    "n_gaps_per_beat_min", "n_gaps_per_beat_max",
    "n_gaps_per_beat_med",
    "width_avg", "width_std",
    "width_min", "width_max",
    "width_med",
    "harmonicity_avg", "harmonicity_std",
    "harmonicity_min", "harmonicity_max",
    "harmonicity_med",
    # Intervals
    "harmonic_intervals_3_6",
    "harmonic_intervals_4_5",
    "harmonic_intervals_8",
    "melodic_intervals_2",
    "melodic_intervals_3",
    "melodic_intervals_4_5",
    "melodic_intervals_8",
    "repetitions",
]



#############
# Functions #
#############

def get_samples(label, pieces=PIECES,
        descriptors_dir=DESCRIPTORS_DIRECTORY,
        ground_truth_dir=GROUND_TRUTH_DIRECTORY,
        feature_names=FEATURE_NAMES):
    """
    Parameters
    ----------
    label : str
        Name of the textural element to be predicted
        in textural elements files.
    pieces : list of str
        Name of the pieces (in PIECES) which are
        put in the dataset

    Returns
    -------
    x : features (n_samples, n_features)
    y : label (n_samples,)
    identifiers : corresponding identifier : piece-measure
    """
    descriptors_files = [
        os.path.join(descriptors_dir, piece + "_descriptors.tsv")
        for piece in pieces
    ]
    ground_truth_files = [
        os.path.join(ground_truth_dir, piece + ".tsv")
        for piece in pieces
    ]

    x_list = []
    y_list = []
    identifiers = []
    for piece, descriptors_file, ground_truth_file in zip(pieces,
            descriptors_files, ground_truth_files):
        
        # Read files
        with open(descriptors_file) as df:
            descriptors = csv.DictReader(df, delimiter="\t")
            descriptors_dict = {
                descriptor["m-measure_number"]: [
                    float(descriptor[key])
                    for key in feature_names
                ]
                for descriptor in descriptors
            }
        with open(ground_truth_file) as gtf:
            textural_elements = csv.DictReader(gtf, delimiter="\t")
            textural_elements_dict = {
                ground_truth["mn"]: ground_truth[label]
                for ground_truth in textural_elements
            }

        # Iterate on measures
        for measure_number in textural_elements_dict:
            if measure_number in descriptors_dict:
                x_list.append(descriptors_dict[measure_number])
                y_list.append(textural_elements_dict[measure_number])
                identifiers.append(
                    "{} m.{}".format(piece, measure_number)
                )

    return x_list, np.array(y_list), identifiers
