"""
This file is part of "Symbolic Texture Dataset" http://algomus.fr/code 
Copyright (C) 2021-2023 by Louis Couturier, 
louis.couturier@u-picardie.fr

"Symbolic Texture Dataset" is free software: you can redistribute it 
and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of 
the License, or (at your option) any later version.

"Symbolic Texture Dataset" is distributed in the hope that it will be 
useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
General Public License for more details.

You should have received a copy of the GNU General Public License 
along with "Symbolic Texture Dataset". If not, see
http://www.gnu.org/licenses/
"""

import csv
import os
import re

from textureclass import Texture


####################
# Global variables #
####################

PIECES = [
    "K279-1",
    "K279-2",
    "K279-3",
    "K280-1",
    "K280-2",
    "K280-3",
    "K283-1",
    "K283-2",
    "K283-3",
]

TXT_DIRECTORY = os.path.join("dataset", "txt")
TSV_DIRECTORY = os.path.join("dataset", "tsv")
#DEZ_DIRECTORY = os.path.join(".", "dez")

TXT_PATHS = [
    os.path.join(TXT_DIRECTORY, piece + ".txt")
    for piece in PIECES
]
#DEZ_PATHS = [
#    os.path.join(DEZ_DIRECTORY, piece + ".dez")
#    for piece in PIECES
#]

TEXTURAL_ELEMENTS = [
    'M', 'H', 'S',
    'h', 'p', 'o',
    'h+', 'p+',
    's', 't', 'b', 'r',
    '_', ',',
    #'global density', 'diversity'
]


#############
# Functions #
#############

def txt_to_tsv(
        file_txt,
        output_dir=TSV_DIRECTORY,
        textural_elements=TEXTURAL_ELEMENTS):
    """
    Create a TSV file from the annotations in given text file.

    Generated file keeps the name of the input file, with a 
    .tsv extension.

    Parameters
    ----------
    file_txt : str or Path object
        Path th
    output_directory : str or Path object
        Path to the directory in which 
    textural_elements : list of str
        List of all textural elements to be computed and saved in the
        output file.
    """
    # Retrieve annotation
    with open(file_txt) as data:
        lines = data.read().split('\n')

    # Build
    textures = []
    for line in lines:
        regex_match = re.match( # * <measure> : <texture> [#<comments>]
            r"^[\s*]*(\d+)\s*:\s*([\w()\[\]/]+(, ?[\w()\[\]/]+){0,1})\s*(#(.*))?$",
            line
        )
        if regex_match: # Parse only 'texture' lines, ignore the rest
            measure_number = int(regex_match.group(1))
            texture = Texture(regex_match.group(2))
            #comment = regex_match.group(5)

            row = {
                'mn': measure_number, # From 1 to N; 0 for anacrusis
                'label': str(texture) # Full texture annotation
            }

            # Compute and save all individual textural elements
            if 'M' in textural_elements:
                row['M'] = int(texture.has_melodic())
            if 'H' in textural_elements:
                row['H'] = int(texture.has_harmonic())
            if 'S' in textural_elements:
                row['S'] = int(texture.has_static())
            if 'h' in textural_elements:
                row['h'] = int(texture.has_attribute('h'))
            if 'p' in textural_elements:
                row['p'] = int(texture.has_attribute('p'))
            if 'o' in textural_elements:
                row['o'] = int(texture.has_attribute('o'))
            if 'h+' in textural_elements:
                row['h+'] = int(texture.has_homorhythmy())
            if 'p+' in textural_elements:
                row['p+'] = int(texture.has_parallel_motions()) 
            if 's' in textural_elements:
                row['s'] = int(texture.has_attribute('s'))
            if 't' in textural_elements:
                row['t'] = int(texture.has_attribute('t'))
            if 'b' in textural_elements:
                row['b'] = int(texture.has_attribute('b'))
            if 'r' in textural_elements:
                row['r'] = int(texture.has_attribute('r'))
            if '_' in textural_elements:
                row['_'] = int(texture.has_attribute('_'))
            if ',' in textural_elements:
                row[','] = int(texture.is_sequential())
            textures.append(row)

    # Save rows
    output_path = os.path.join(
        output_dir, # Directory
        os.path.basename(file_txt).replace(".txt", ".tsv") # New file name
    )
    with open(output_path, 'w', newline='\n') as tsv_file:
        writer = csv.DictWriter(
            tsv_file,
            fieldnames=['mn', 'label']+textural_elements,
            delimiter='\t'
        )
        writer.writeheader()
        for texture in textures:
            writer.writerow(texture)
    print("File saved")

def txt_to_tsv_many(
        file_txt_list=TXT_PATHS,
        output_dir=TSV_DIRECTORY,
        textural_elements=TEXTURAL_ELEMENTS):
    """
    Convert all given text files to TSV files.
    
    Generated files keep the name of input files, with a 
    .tsv extension.

    Parameters
    ----------
    file_txt : str or Path object
        Path th
    output_directory : str or Path object
        Path to the directory in which 
    textural_elements : list of str
        List of all textural elements to be computed and saved in the
        output file.
    """
    for path in file_txt_list:
        print("Parsing", str(path))
        txt_to_tsv(
            file_txt=path,
            output_dir=output_dir,
            textural_elements=textural_elements
        )



if __name__ == "__main__":
    # Convert single TXT file to TSV (with textural elements)
    #txt_to_tsv(os.path.join(TXT_DIRECTORY, "K279-1.txt"))

    # Convert all TXT to TSV
    txt_to_tsv_many()
