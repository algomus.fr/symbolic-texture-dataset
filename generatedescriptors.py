"""
This module aims at extracting musical descriptors from notelist
TSV files (https://github.com/DCMLab/mozart_piano_sonatas/tree/main/notes)
and build TSV files containing this information.

License
-------

This file is part of "Symbolic Texture Dataset" http://algomus.fr/code 
Copyright (C) 2021-2023 by Louis Couturier, 
louis.couturier@u-picardie.fr

"Symbolic Texture Dataset" is free software: you can redistribute it 
and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of 
the License, or (at your option) any later version.

"Symbolic Texture Dataset" is distributed in the hope that it will be 
useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
General Public License for more details.

You should have received a copy of the GNU General Public License 
along with "Symbolic Texture Dataset". If not, see
http://www.gnu.org/licenses/
"""

import csv
import os

from descriptors import Pitches, Onsets, Slices  # to extract descriptors



####################
# Global variables #
####################

PIECES = [
    "K279-1",
    "K279-2",
    "K279-3",
    "K280-1",
    "K280-2",
    "K280-3",
    "K283-1",
    "K283-2",
    "K283-3",
]

INPUT_DIRECTORY = os.path.join("..", "mozart_piano_sonatas", "notes")
INPUT_PATH_LIST =  [
    os.path.join(INPUT_DIRECTORY, piece + ".tsv")
    for piece in PIECES
]

OUTPUT_DIRECTORY = "descriptors"
OUTPUT_FILENAMES = [
    piece + "_descriptors.tsv"
    for piece in PIECES
]
OUTPUT_PATH_LIST = [
    os.path.join(OUTPUT_DIRECTORY, filename)
    for filename in OUTPUT_FILENAMES
]



#############
# Functions #
#############

def extract_notes(filename, *measure_numbers):
    """
    Return all the notes dictionaries in given
    measures from given piece

    Parameters
    ----------
    filename: str
        Path to target TSV file to be parsed
    measure_number: int
        Measure number ('mn' as defined in https://github.com/DCMLab/mozart_piano_sonatas/tree/main/notes)
        in which the notes need to be extracted. If none is given,
        retrieve all the notes.

    Returns
    -------
    list of dict
        The list of all the notes in targeted measures
    """
    # Check file path
    if not os.path.exists(filename):
        #return ValueError("File not found.")
        print("File not found:", filename)
        return []

    with open(filename) as tsv_file:
        reader = csv.DictReader(tsv_file, delimiter='\t')
        if measure_numbers:
            return [note for note in reader if int(note['mn']) in measure_numbers]
        else:
            return [note for note in reader]



def extract_descriptors(note_list, output_file="test.tsv", verbose=True):
    """
    Parameters
    ----------
    note_list : list of dict
        List of note dictionaries as extracted in TSV representation #TODO: details
    output_file : str
        path to the csv file in which the information
        will be written.
    """
    meta_information = [
        "m-measure_number",
        "m-length_beat",
        "m-beat_duration"
    ]
    descriptor_names = [
        # Unary descriptors
        "has_even_meter",
        "n_notes",
        "n_pitches",
        "n_onsets",
        "n_slices",
        "n_pitchclasses",
        "novelty",
        "prop_silence",
        "longest_silence",
        # Composed descriptors
        "pitch_avg", "pitch_std",
        "pitch_min", "pitch_max",
        "pitch_med",
        "duration_avg", "duration_std",
        "duration_min", "duration_max",
        "duration_med",
        "regularity_avg", "regularity_std",
        "regularity_min", "regularity_max",
        "regularity_med",
        "n_voices_per_onset_avg", "n_voices_per_onset_std",
        "n_voices_per_onset_min", "n_voices_per_onset_max",
        "n_voices_per_onset_med",
        "n_gaps_per_onset_avg", "n_gaps_per_onset_std",
        "n_gaps_per_onset_min", "n_gaps_per_onset_max",
        "n_gaps_per_onset_med",
        "n_voices_per_beat_avg", "n_voices_per_beat_std",
        "n_voices_per_beat_min", "n_voices_per_beat_max",
        "n_voices_per_beat_med",
        "n_gaps_per_beat_avg", "n_gaps_per_beat_std",
        "n_gaps_per_beat_min", "n_gaps_per_beat_max",
        "n_gaps_per_beat_med",
        "width_avg", "width_std",
        "width_min", "width_max",
        "width_med",
        "harmonicity_avg", "harmonicity_std",
        "harmonicity_min", "harmonicity_max",
        "harmonicity_med",
        # Intervals
        "harmonic_intervals_3_6",
        "harmonic_intervals_4_5",
        "harmonic_intervals_8",
        "melodic_intervals_2",
        "melodic_intervals_3",
        "melodic_intervals_4_5",
        "melodic_intervals_8",
        "repetitions",
    ]

    # Loop on data
    all_measures = list(set([int(note["mn"]) for note in note_list]))
    all_measures.sort()

    descriptors_list = []
    for i_measure in all_measures:
        descriptors = {}

        if verbose:
            print("Measure", i_measure, end='\r')
        # Select only current measure
        notes = [
            note for note in note_list
            if int(note['mn']) == i_measure
        ]

        # Build intermediary structures
        pitches = Pitches(notes)
        beat_duration = pitches.beat_duration
        length_quarter = pitches.length_quarter
        length_beat = pitches.length_beat
        onsets = Onsets(
            notes,
            length=length_quarter,
            beat_duration=beat_duration
        )
        slices = Slices(
            notes,
            length=length_quarter,
            beat_duration=beat_duration
        )

        # Add meta information
        descriptors['m-measure_number'] = i_measure
        descriptors['m-length_beat'] = length_beat
        descriptors['m-beat_duration'] = beat_duration

        # Add descriptors
        ## Unary descriptors
        descriptors['has_even_meter'] = int(slices.has_even_meter())
        descriptors['n_notes'] = pitches.n_notes()
        descriptors['n_pitches'] = pitches.n_pitches()
        descriptors['n_onsets'] = onsets.n_onsets()
        descriptors['n_slices'] = slices.n_slices()
        descriptors['n_pitchclasses'] = pitches.n_pitchclasses()
        descriptors['novelty'] = pitches.novelty()
        descriptors['prop_silence'] = slices.prop_silence()
        descriptors['longest_silence'] = slices.longest_silence()

        # Composed descriptors
        composed_descriptors = {
            'pitch': pitches.stats_pitches(),
            'duration': pitches.stats_durations(),
            'regularity': onsets.stats_regularity(),
            'width': slices.stats_width(),
            'n_voices_per_onset': onsets.stats_n_voices(),
            'n_voices_per_beat': slices.stats_n_voices(),
            'n_gaps_per_onset': onsets.stats_n_gaps(),
            'n_gaps_per_beat': slices.stats_n_gaps(),
            'harmonicity': slices.stats_harmonicity()
        }
        for descriptor_name, values in composed_descriptors.items():
            if None in values:
                descriptors[descriptor_name+"_avg"] = None
                descriptors[descriptor_name+"_std"] = None
                descriptors[descriptor_name+"_min"] = None
                descriptors[descriptor_name+"_max"] = None
                descriptors[descriptor_name+"_med"] = None
            else:
                avg, std, min_v, max_v, med = values
                descriptors[descriptor_name+"_avg"] = float(avg)
                descriptors[descriptor_name+"_std"] = float(std)
                descriptors[descriptor_name+"_min"] = float(min_v)
                descriptors[descriptor_name+"_max"] = float(max_v)
                descriptors[descriptor_name+"_med"] = float(med)

        # Intervalic descriptors
        hi_3_6, hi_4_5, hi_8 = onsets.harmonic_intervals()
        descriptors['harmonic_intervals_3_6'] = hi_3_6
        descriptors['harmonic_intervals_4_5'] = hi_4_5
        descriptors['harmonic_intervals_8'] = hi_8
        descriptors['melodic_intervals_2'] = slices.melodic_intervals(
            "s", onsets.onset_dict)
        descriptors['melodic_intervals_3'] = slices.melodic_intervals(
            "t", onsets.onset_dict)
        descriptors['melodic_intervals_4_5'] = slices.melodic_intervals(
            "f", onsets.onset_dict)
        descriptors['melodic_intervals_8'] = slices.melodic_intervals(
            "o", onsets.onset_dict)
        descriptors['repetitions'] = slices.melodic_intervals(
            "u", onsets.onset_dict)
        
        descriptors_list.append(descriptors) # Save descriptors for this measure

    # Save results in TSV file

    with open(output_file, 'w', newline='\n') as tsv_file:
        writer = csv.DictWriter(
            tsv_file,
            fieldnames=meta_information+descriptor_names,
            delimiter='\t'
        )
        writer.writeheader()
        for descriptors in descriptors_list:
            writer.writerow(descriptors)



def extract_all_descriptors(
        input_files=INPUT_PATH_LIST, output_files=OUTPUT_PATH_LIST,
        verbose=True):
    for input_file, output_file in zip(input_files, output_files):
        if verbose:
            print("### Computing descriptors in", os.path.basename(input_file))
        note_list = extract_notes(input_file)
        extract_descriptors(
            note_list= note_list,
            #input_file=input_file, #TODO: directly take the filename
            output_file=output_file,
            verbose=verbose
        )
        if verbose:
            print("\rSaved at ", output_file)




if __name__ == "__main__":
    ## Extract descriptors
    # -> Uncomment this section to extract descriptors for one file
    """
    note_list = extract_notes(INPUT_PATH_LIST[0])
    extract_descriptors(
        note_list=note_list,
        output_file=OUTPUT_PATH_LIST[0]
    )
    """
    

    ## Extract all
    # -> Uncomment this line to extract all descriptors
    extract_all_descriptors()

    pass
